import chroma from "chroma-js";
import {Rectangle} from "../base/Rectangle.js";
import {Constants} from "../base/Constants.js";
import {BarLayout} from "./BarLayout.js";
import {Axis, EditInput} from "./Axis.js";
import {Util} from "../base/Util.js";
import {Animatable} from "../base/Animatable.js";
import {LOD} from "./LOD.js";
import {DataTable} from "../base/DataTable.js";

export class BarChart {

    static COLORS = chroma.brewer.Set2;

    #viewport;
    #backcolor;

    #data;

    #_bounds;
    #bounds;

    #axes;

    #pickedBar = null;
    #pickedAttr = -1;

    #layouts;
    #activeLayout;

    #cellEntity;

    constructor(graph, viewport, refresh, backcolor = () => '#00000000', cellEntity = 0) {

        this.editInput = new EditInput(refresh);

        this.#viewport = viewport;
        this.#backcolor = backcolor;

        this.#_bounds = new Rectangle(-Constants.LAYOUT_EXTENT / 2, -Constants.LAYOUT_EXTENT / 2, Constants.LAYOUT_EXTENT, Constants.LAYOUT_EXTENT); // In world space
        this.#bounds = new Rectangle(); // In view space

        this.#layouts = [new BarLayout(this, BarLayout.HORIZONTAL), new BarLayout(this, BarLayout.VERTICAL)];
        this.#activeLayout = 0;

        this.#cellEntity = cellEntity;

        this.setdata(DataTable.emptyData());
    }

    get axes() {
        return this.#axes;
    }

    get data() {
        return this.#data;
    }

    get activeLayout() {
        return this.#activeLayout;
    }

    set activeLayout(l) {
        this.#activeLayout = Util.keepModulo(l, this.#layouts.length);
        this.layout.invalidate();
    }

    get layout() {
        return this.#layouts[this.#activeLayout];
    }

    setdata(data) {

        this.#data = data;
        this.#axes = data.attributes.map((attr, i) => new Axis(attr, data.labels[i], ...Util.autoExpand(0, data.max[attr])));

        this.groups = data.attributes.map((attr, i) => ({
            attribute: i,
            _bounds: new Rectangle(),
            bounds: new Rectangle()
        }));

        this.bars = this.groups.map((grp, i) =>
            data.tuples.map((tuple, j) => ({
                attribute: i,
                tuple: j,
                _anchor: new Animatable([0, 0]),
                anchor: [0, 0],
            }))
        );

        this.angle = new Animatable([0]);

        this.layout.invalidate();
    }

    pick(evt) {

        if (this.#pickedBar != null) {
            this.#data.entities[this.#pickedBar.tuple].highlight.delete(this);
            if (this.#cellEntity !== 0) {
                this.#data.entities[this.#pickedBar.tuple].source.highlight.delete(this);
                this.#data.entities[this.#pickedBar.tuple].target.highlight.delete(this);
            }
            this.#pickedBar = null;
            this.#pickedAttr = -1;
        }

        const [x, y] = evt.screenCoords;
        const barvector = this.layout.vector(); // Vector in the direction of the bar [0,1] (horizontal) or [1,0] (vertical)
        const barspace = Util.length([barvector[1] * this.#bounds.width, barvector[0] * this.#bounds.height]); // How much space is there for bars
        const barwidth = Math.max(1, Math.min(30, 0.9 * barspace / this.#data.attributes.length / this.#data.tuples.length));

        this.groups.forEach((grp, i) => {

            if (x < grp.bounds.left || x > grp.bounds.right || y < grp.bounds.top || y > grp.bounds.bottom) return;

            for (const {bar, a, b} of this.barlines({inverse: true, group: i})) {
                let d = Util.distance(evt.screenCoords, Util.projectPointToLine(evt.screenCoords, a, b));
                if (d < .5 * barwidth) {
                    this.#pickedBar = bar;
                    this.#data.entities[bar.tuple].highlight.add(this);
                    if (this.#cellEntity !== 0) {
                        this.#data.entities[bar.tuple].source.highlight.add(this);
                        this.#data.entities[bar.tuple].target.highlight.add(this);
                    }

                    d = Util.distanceSq(evt.screenCoords, b);
                    if (d < Constants.PICK_TOLERANCE_SQ) {
                        this.#pickedAttr = bar.attribute;
                    }

                    break;
                }
            }
        });

        return this.#pickedBar != null;
    }

    onmousedown(evt) {
        if (evt.button === 0) {

            // Drag must be left button
            this.drag = {
                down: evt.screenCoords, // Coordinates where drag started
                dragged: false, // Actually dragged after threshold has been reached ?
                edit: false
            };

            // Click must be left button
            this.click = {
                down: evt.screenCoords // Coordinates where drag started
            };

            //Close edit input
            this.editInput.hide();

            if (this.#pickedBar != null && this.#pickedAttr !== -1) { // Special drag for selecting entire rows
                this.drag.edit = true;
                this.drag.bar = this.#pickedBar;

                this.click.edit = true;
                this.click.bar = this.#pickedBar;
            }
            return true; // Event consumed
        }
        return false;
    }

    onmouseup(evt) {
        if (this.drag && this.drag.dragged) { // Could this be a drag
            if (this.drag.edit) { // Has the drag been actually performed

                // Check and update axis min/max
                let max = this.#data.max[this.#data.attributes[this.drag.bar.attribute]];
                this.#axes[this.drag.bar.attribute].minmax(...Util.autoExpand(0, max));
            }
            delete this.drag;
            delete this.click;

            return true; // Event consumed
        }

        if (this.click) {
            if (this.click.edit) {
                // Read value of tuple
                let value = this.#data.tuples[this.click.bar.tuple][this.click.bar.attribute];

                // Show input field
                this.editInput.show([evt.clientX, evt.clientY], this, this.click.bar.attribute, this.click.bar.tuple, value);
            }

            delete this.drag;
            delete this.click;

            return true; // Event consumed
        }

        return false;
    }

    onmousemove(evt) {
        if (this.drag) { // Could this be a drag

            // Only consider a drag when the cursor has been moved far enough
            const dx = this.drag.down[0] - evt.screenCoords[0];
            const dy = this.drag.down[1] - evt.screenCoords[1];
            this.drag.dragged = (this.drag.dragged || dx >= Constants.DRAG_THRESHOLD || dx < -Constants.DRAG_THRESHOLD || dy >= Constants.DRAG_THRESHOLD || dy < -Constants.DRAG_THRESHOLD);

            if (this.drag.dragged && this.drag.edit) {
                if (evt.ctrlKey) {
                    this.#data.tuples[this.drag.bar.tuple][this.drag.bar.attribute] = Number(this.#axes[this.drag.bar.attribute].unmap(evt.screenCoords).toFixed(3));
                } else {
                    this.#data.tuples[this.drag.bar.tuple][this.drag.bar.attribute] = Number(this.#axes[this.drag.bar.attribute].unmap(evt.screenCoords).toFixed(0));
                }
                this.editInput.hide();
            }
        }
        return true; // Event consumed
    }

    onkeydown(evt) {
        switch (evt.key) {
            case "Escape":
                this.editInput.hide();
                return true;
        }
        return false; // Event not consumed
    }

    onclose() {
        this.#data.entities.forEach(e => e.highlight.delete(this));
        if (this.#cellEntity !== 0) {
            this.#data.entities.forEach(e => {
                e.source.highlight.delete(this);
                e.target.highlight.delete(this);
            });
        }

        //Close edit input
        this.editInput.hide();
    }

    editTuple(tuple, attr, value) {
        this.#data.tuples[tuple][attr] = value;
    }

    update(time) {
        let needUpdate;

        needUpdate = this.layout.update(time);

        this.#axes.forEach(axis => {
            // TODO: Avoid setting min/max on every frame
            axis.minmax(...Util.autoExpand(this.#data.min[axis.attribute], this.#data.max[axis.attribute]));
            needUpdate = axis.update(time) || needUpdate;
        });

        this.bars.forEach(grp => {
            grp.forEach(bar => {
                needUpdate = bar._anchor.update(time) || needUpdate;
            })
        });

        needUpdate = this.angle.update(time) || needUpdate;

        this.map(); // TODO: Avoid mapping tuples on every frame
        this.project();

        this.LOD = LOD.lod(Math.min(this.#bounds.width, this.#bounds.height));

        return needUpdate;
    }

    map() {
        this.bars.forEach((grp, i) => {
            let axis = this.#axes[i];
            grp.forEach(bar => {
                let val = this.#data.tuples[bar.tuple][bar.attribute];
                bar.normalizedlength = axis.mapParametric(val);
            })
        });
    }

    project() {

        this.#bounds.set(...this.#viewport.project(this.#_bounds));

        this.#axes.forEach(axis => {
            axis.start = this.#viewport.project(axis._start.value);
            axis.end = this.#viewport.project(axis._end.value);
        });

        this.groups.forEach(grp => {
            grp.bounds.set(...this.#viewport.project(grp._bounds));
        });

        this.bars.forEach(grp => {
            grp.forEach(bar => {
                bar.anchor = this.#viewport.project(bar._anchor.value);
            })
        });
    }

    drawAxes(gc) {
        if (this.LOD < LOD.COMPACT) return;
        Axis.drawAxes(gc, this.#axes, this.#backcolor, this.#bounds, this.LOD, this.#pickedAttr);
    }

    * barrects(options = {}) { // Generator function to create bar rectangles
        const barvector = this.layout.vector(); // Vector in the direction of the bar [0,1] (horizontal) or [1,0] (vertical)
        const barlength = Util.length([barvector[0] * this.#bounds.width, barvector[1] * this.#bounds.height]); // How long can a bar be
        const barspace = Util.length([barvector[1] * this.#bounds.width, barvector[0] * this.#bounds.height]); // How much space is there for bars
        const barwidth = Math.max(1, Math.min(30 * window.devicePixelRatio, 0.9 * barspace / this.#data.attributes.length / this.#data.tuples.length));

        const rect = (i, j) => {
            let bar = this.bars[i][j];
            let [x, y, w, h] = [0, -.5 * barwidth, bar.normalizedlength * barlength, barwidth];
            return {bar, x, y, w, h};
        };

        if (options.inverse) {
            const start = (options.group !== undefined) ? options.group : this.bars.length - 1;
            const end = (options.group !== undefined) ? options.group : 0;
            for (let i = start; i >= end; i--) {
                for (let j = this.bars[i].length - 1; j >= 0; j--) {
                    yield rect(i, j);
                }
            }
        } else {
            const start = (options.group !== undefined) ? options.group : 0;
            const end = (options.group !== undefined) ? options.group : this.bars.length;
            for (let i = start; i < end; i++) {
                for (let j = 0; j < this.bars[i].length; j++) {
                    yield rect(i, j);
                }
            }
        }
    }

    drawBarRects(gc) {
        const contrastcolor = Util.contrastColor(this.#backcolor());
        const barangle = this.angle.value[0];
        const picked = [];
        gc.strokeStyle = '#333';
        gc.lineWidth = .5;

        for (const {bar, x, y, w, h} of this.barrects()) {
            if (this.#data.entities[bar.tuple].highlight.size > 0) {
                picked.push({bar, x, y, w, h});
            }
            gc.save();
            gc.translate(...bar.anchor);
            gc.rotate(barangle);
            gc.fillStyle = (this.LOD > LOD.MINIATURE) ? BarChart.COLORS[bar.attribute % BarChart.COLORS.length] : contrastcolor;
            gc.fillRect(x, y, w, h);
            if (h > 2) {
                gc.strokeRect(x, y, w, h);
            }
            gc.restore();
        }

        for (const {bar, x, y, w, h} of picked) {
            gc.save();
            gc.translate(...bar.anchor);
            gc.rotate(barangle);
            gc.fillStyle = 'red';
            gc.fillRect(x, y, w, h);
            if (h > 2) {
                gc.strokeRect(x, y, w, h);
            }
            gc.restore();
        }
    }

    * barlines(options = {}) { // Generator function to create bar lines
        const barvector = this.layout.vector(); // Vector in the direction of the bar [0,1] (horizontal) or [1,0] (vertical)
        const barlength = Util.length([barvector[0] * this.#bounds.width, barvector[1] * this.#bounds.height]); // How long can a bar be
        const barangle = this.angle.value[0];
        const cos = Math.cos(barangle);
        const sin = Math.sin(barangle);

        const line = (i, j) => {
            let bar = this.bars[i][j];
            let a = [bar.anchor[0], bar.anchor[1]];
            let b = [barlength * bar.normalizedlength, 0];
            b = [b[0] * cos - b[1] * sin, b[0] * sin + b[1] * cos]; // Rotate
            b = [b[0] + bar.anchor[0], b[1] + bar.anchor[1]]; // Translate
            return {bar, a, b};
        };

        if (options.inverse) {
            const start = (options.group !== undefined) ? options.group : this.bars.length - 1;
            const end = (options.group !== undefined) ? options.group : 0;
            for (let i = start; i >= end; i--) {
                for (let j = this.bars[i].length - 1; j >= 0; j--) {
                    yield line(i, j);
                }
            }
        } else {
            const start = (options.group !== undefined) ? options.group : 0;
            const end = (options.group !== undefined) ? options.group : this.bars.length;
            for (let i = start; i < end; i++) {
                for (let j = 0; j < this.bars[i].length; j++) {
                    yield line(i, j);
                }
            }
        }
    }

    drawBarLines(gc) {
        const contrastcolor = Util.contrastColor(this.#backcolor());
        const barvector = this.layout.vector(); // Vector in the direction of the bar [0,1] (horizontal) or [1,0] (vertical)
        const barspace = Util.length([barvector[1] * this.#bounds.width, barvector[0] * this.#bounds.height]); // How much space is there for bars
        const barwidth = Math.max(0.5, Math.min(30, 0.9 * barspace / this.#data.attributes.length / this.#data.tuples.length)) * window.devicePixelRatio;
        const picked = [];

        gc.save();
        gc.lineWidth = barwidth;
        for (const {bar, a, b} of this.barlines()) {
            if (this.#data.entities[bar.tuple].highlight.size > 0) {
                picked.push({bar, a, b});
            }
            gc.strokeStyle = (this.LOD > LOD.MINIATURE) ? BarChart.COLORS[bar.attribute % BarChart.COLORS.length] : contrastcolor;
            gc.beginPath();
            gc.moveTo(...a);
            gc.lineTo(...b);
            gc.stroke();
        }
        gc.restore();

        gc.save();
        gc.strokeStyle = 'red';
        gc.lineWidth = barwidth;

        for (const {bar, a, b} of picked) {
            gc.beginPath();
            gc.moveTo(...a);
            gc.lineTo(...b);
            gc.stroke();

            if (this.LOD > LOD.COMPACT && bar.attribute === this.#pickedAttr) {
                gc.save();
                gc.strokeStyle = 'white';
                gc.fillStyle = 'red';
                gc.lineWidth = 1;
                gc.beginPath();
                gc.arc(...b, Constants.PICK_TOLERANCE, 0, 2 * Math.PI);
                gc.fill();
                gc.stroke();
                gc.restore();
            }
        }
        gc.restore();
    }

    drawTupleValues(gc) {
        for (const {bar, a, b} of this.barlines()) {
            if (this.#data.entities[bar.tuple].highlight.size > 0) {
                gc.save();
                const extent = Math.min(this.#bounds.width, this.#bounds.height) / window.devicePixelRatio;
                const size = Util.font(gc, 'sans-serif', 4, .04 * extent, 12, 12, '#909090');

                const value = this.#data.tuples[bar.tuple][bar.attribute];

                let isHorizontal = this.layout.vector() === BarLayout.BAR_VECTOR[BarLayout.HORIZONTAL];
                if (isHorizontal) {
                    gc.translate(a[0], a[1])
                    gc.fillText(value, -gc.measureText(value).width * .5, size);
                } else {
                    gc.translate(a[0], a[1])
                    gc.rotate(-0.5 * Math.PI);
                    gc.fillText(value, -gc.measureText(value).width * .5, -size * .25);
                }

                gc.restore();
            }
        }
    }

    drawBars(gc) {
        // this.drawBarRects(gc);
        this.drawBarLines(gc);
    }

    drawLabels(gc) {
        if (this.LOD < LOD.COMPACT) return;
        Axis.drawLabels(gc, this.#axes, this.#bounds);
        Axis.drawAxesValues(gc, this.#axes, this.#bounds);
        this.drawTupleValues(gc);
    }

    draw(gc) {
        this.drawAxes(gc);
        this.drawBars(gc);
        this.drawLabels(gc);
    }

    bounds() {
        return this.#_bounds;
    }
}