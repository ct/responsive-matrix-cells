import {AxesLayout} from "./AxesLayout.js";
import {Rectangle} from "../base/Rectangle.js";
import {Constants} from "../base/Constants.js";
import {Axis, EditInput} from "./Axis.js";
import {Util} from "../base/Util.js";
import {LOD} from "./LOD.js";
import {DataTable} from "../base/DataTable.js";

export class AxesPlot {

    #viewport;
    #backcolor;

    #data;

    #_bounds;
    #bounds;

    #axes;
    #tuples;
    #polylines;

    #pickedAxis = -1;
    #pickedTuple = -1;

    #layouts;
    #activeLayout;

    constructor(graph, viewport, refresh, backcolor = () => '#00000000', cellEntity = 0) {

        this.editInput = new EditInput(refresh);

        this.#viewport = viewport;
        this.#backcolor = backcolor;

        this.#_bounds = new Rectangle(-Constants.LAYOUT_EXTENT / 2, -Constants.LAYOUT_EXTENT / 2, Constants.LAYOUT_EXTENT, Constants.LAYOUT_EXTENT); // In world space
        this.#bounds = new Rectangle(); // In view space

        this.#layouts = [new AxesLayout(this, AxesLayout.HORIZONTAL_PCP), new AxesLayout(this, AxesLayout.VERTICAL_PCP), new AxesLayout(this, AxesLayout.STAR)];
        this.#activeLayout = 2;

        this.setdata(DataTable.emptyData());
    }

    get axes() {
        return this.#axes;
    }

    get tuples() {
        return this.#tuples;
    }

    get activeLayout() {
        return this.#activeLayout;
    }

    set activeLayout(l) {
        this.#activeLayout = Util.keepModulo(l, this.#layouts.length);
        this.layout.invalidate();
    }

    get layout() {
        return this.#layouts[this.#activeLayout];
    }

    setdata(data) {

        this.#data = data;
        this.#axes = data.attributes.map((attr, i) => new Axis(attr, data.labels[i], ...Util.autoExpand(data.min[attr], data.max[attr])));
        this.#tuples = data.tuples;
        this.#polylines = data.tuples.map(tuple => ({
            _points: data.attributes.map(a => [0, 0]),
            points: data.attributes.map(a => [0, 0])
        }));

        this.layout.invalidate();
    }

    pick(evt) {
        let closestAxes = [-1, -1]; // Store the closest and the second-closest axes
        let min = [Number.POSITIVE_INFINITY, Number.POSITIVE_INFINITY]; // Need to memorize smallest distance and second-smallest distance

        this.#pickedAxis = -1;
        if (this.#pickedTuple !== -1) {
            this.#data.entities[this.#pickedTuple].highlight.delete(this);
            this.#pickedTuple = -1;
        }

        const [x, y] = evt.screenCoords;
        if (x < this.#bounds.left || x > this.#bounds.right || y < this.#bounds.top || y > this.#bounds.bottom) return false; // Quickly return if outside bounds

        // Pick axes
        for (let i = this.#axes.length - 1; i >= 0; i--) { // Picking order should be inverse of drawing order
            let d = this.#axes[i].distanceSq(evt.screenCoords);
            if (d < min[0]) { // If distance is smaller than first minimum
                min[1] = min[0]; // Update second minimum ..
                closestAxes[1] = closestAxes[0]; // .. and second axis
                min[0] = d; // Store new first minimum ..
                closestAxes[0] = i; // and first axis
            } else if (d < min[1]) { // If distance is smaller than the second minimum
                min[1] = d; // Store new second minimum ..
                closestAxes[1] = i; // .. and second axis
            }
        }
        if (min[0] < Constants.PICK_TOLERANCE_SQ) { // If minimum distance is below tolerance
            this.#pickedAxis = closestAxes[0];
        }
        if (closestAxes[1] === -1) closestAxes[1] = closestAxes[0];

        let closestTuple = -1;
        let mini = Number.POSITIVE_INFINITY;

        // Pick tuples
        if (this.#pickedAxis !== -1) {
            // Pick closest tuple point on the axis
            for (let i = this.#tuples.length - 1; i >= 0; i--) { // Picking order should be inverse of drawing order
                let d = Util.distanceSq(evt.screenCoords, this.#polylines[i].points[this.#pickedAxis]);

                if (d < mini) {
                    mini = d;
                    closestTuple = i;
                }
            }
        }
        if (mini < Constants.PICK_TOLERANCE_SQ) {
            this.#pickedTuple = closestTuple;
            this.#data.entities[this.#pickedTuple].highlight.add(this);
        }

        if (this.#pickedTuple === -1) { // If no closest point on axis has been found
            // Pick closest tuple line between two axes
            for (let i = this.#tuples.length - 1; i >= 0; i--) { // Picking order should be inverse of drawing order

                // Only check lines between the two axes that are closest to the cursor
                let a = this.#polylines[i].points[closestAxes[0]];
                let b = this.#polylines[i].points[closestAxes[1]];

                let d = Util.distanceSq(evt.screenCoords, Util.projectPointToLine(evt.screenCoords, a, b));

                if (d < mini) {
                    mini = d;
                    closestTuple = i;
                }
            }
        }
        if (mini < Constants.PICK_TOLERANCE_SQ) {
            this.#pickedTuple = closestTuple;
            this.#data.entities[this.#pickedTuple].highlight.add(this);
        }

        // console.log(this.#pickedAxis, this.#pickedTuple);

        return this.#pickedAxis !== -1 || this.#pickedTuple !== -1;
    }

    onmousedown(evt) {
        if (evt.button === 0) {

            // Drag must be left button
            this.drag = {
                down: evt.screenCoords, // Coordinates where drag started
                dragged: false, // Actually dragged after threshold has been reached ?
                edit: false
            };

            // Click must be left button
            this.click = {
                down: evt.screenCoords // Coordinates where drag started
            };

            //Close edit input
            this.editInput.hide();

            if (this.#pickedTuple !== -1 && this.#pickedAxis !== -1) { // Special drag for selecting entire rows
                this.drag.edit = true;
                this.drag.tuple = this.#pickedTuple;
                this.drag.axis = this.#pickedAxis;

                this.click.edit = true;
                this.click.tuple = this.#pickedTuple;
                this.click.axis = this.#pickedAxis;
            }
            return true; // Event consumed
        }
        return false;
    }

    onmouseup(evt) {
        if (this.drag && this.drag.dragged) { // Could this be a drag
            if (this.drag.edit) { // Has the drag been actually performed

                // Check and update axis min/max
                let axis = this.#axes[this.drag.axis];
                let v = this.#tuples[this.drag.tuple][this.drag.axis];
                let min = this.#data.min[axis.attribute];
                let max = this.#data.max[axis.attribute];
                axis.minmax(...Util.autoExpand(min, max));
            }
            delete this.drag;
            delete this.click; // Drag was performed, so this up event should NOT become a click

            return true; // Event consumed
        }

        if (this.click) {
            if (this.click.edit) {
                // Read value of tuple
                let value = this.#tuples[this.click.tuple][this.click.axis];

                // Show input field
                this.editInput.show([evt.clientX, evt.clientY], this, this.click.axis, this.click.tuple, value);
            }

            delete this.drag;
            delete this.click;

            return true; // Event consumed
        }

        return false;
    }

    onmousemove(evt) {
        if (this.drag) { // Could this be a drag

            // Only consider a drag when the cursor has been moved far enough
            const dx = this.drag.down[0] - evt.screenCoords[0];
            const dy = this.drag.down[1] - evt.screenCoords[1];
            this.drag.dragged = (this.drag.dragged || dx >= Constants.DRAG_THRESHOLD || dx < -Constants.DRAG_THRESHOLD || dy >= Constants.DRAG_THRESHOLD || dy < -Constants.DRAG_THRESHOLD);

            if (this.drag.dragged && this.drag.edit) {
                if (evt.ctrlKey) {
                    this.#tuples[this.drag.tuple][this.drag.axis] = Number(this.#axes[this.drag.axis].unmap(evt.screenCoords).toFixed(3));
                } else {
                    this.#tuples[this.drag.tuple][this.drag.axis] = Number(this.#axes[this.drag.axis].unmap(evt.screenCoords).toFixed(0));
                }
                this.editInput.hide();
            }
        }
        return true; // Event consumed
    }

    onkeydown(evt) {
        switch (evt.key) {
            case "Escape":
                this.editInput.hide();
                return true;
        }
        return false; // Event not consumed
    }

    onclose() {
        this.#data.entities.forEach(e => e.highlight.delete(this));

        //Close edit input
        this.editInput.hide();
    }

    editTuple(tuple, attr, value) {
        this.#tuples[tuple][attr] = value;
    }

    update(time) {
        let needUpdate;

        needUpdate = this.layout.update(time);
        this.axes.forEach(axis => {
            // TODO: Avoid setting min/max on every frame
            axis.minmax(...Util.autoExpand(this.#data.min[axis.attribute], this.#data.max[axis.attribute]));
            needUpdate = axis.update(time) || needUpdate;
        });

        this.map(); // TODO: Avoid mapping tuples on every frame
        this.project();

        this.LOD = LOD.lod(Math.min(this.#bounds.width, this.#bounds.height));

        return needUpdate;
    }

    map() {
        this.#polylines.forEach((polyline, i) => {
            this.#axes.forEach((axis, j) => {
                polyline._points[j] = axis.map(this.#tuples[i][j]);
            });
        });
    }

    project() {

        this.#bounds.set(...this.#viewport.project(this.#_bounds));

        this.#axes.forEach(axis => {
            axis.start = this.#viewport.project(axis._start.value);
            axis.end = this.#viewport.project(axis._end.value);
        });

        this.#polylines.forEach(polyline => {
            polyline.points = polyline._points.map(_p => this.#viewport.project(_p));
        });
    }

    drawAxes(gc) {
        Axis.drawAxes(gc, this.#axes, this.#backcolor, this.#bounds, this.LOD, this.#pickedAxis);
    }

    drawHistograms(gc) {
        // TODO: Draw histogram at a higher level of detail
    }

    drawPolylines(gc) {
        const contrastcolor = Util.contrastColor(this.#backcolor());

        const LINE_WIDTH = 1 * window.devicePixelRatio;

        gc.save();
        gc.lineWidth = LINE_WIDTH;
        gc.strokeStyle = (this.LOD > LOD.MINIATURE) ? 'rgba(128, 128, 128, .5)' : contrastcolor;
        gc.fillStyle = 'rgba(200, 200, 200, .1)';
        let highlight = [];
        this.#polylines.forEach((polyline, i) => {
            gc.beginPath();
            gc.moveTo(...polyline.points[0]);
            for (let i = 1; i < polyline.points.length; i++) {
                gc.lineTo(...polyline.points[i]);
            }
            if (this.layout.closed()) {
                gc.closePath();
                if (this.LOD > LOD.MINIATURE) gc.fill();
            }
            gc.stroke();
            if (this.#data.entities[i].highlight.size > 0) highlight.push(i);
        });

        if (highlight.length !== 0) {
            for (let j = 0; j < highlight.length; j++) {
                let polyline = this.#polylines[highlight[j]];
                gc.lineWidth = LINE_WIDTH * 1.5;
                gc.strokeStyle = (this.LOD > LOD.MINIATURE) ? 'red' : contrastcolor;
                gc.beginPath();
                gc.moveTo(...polyline.points[0]);
                for (let i = 1; i < polyline.points.length; i++) {
                    gc.lineTo(...polyline.points[i]);
                }
                if (this.layout.closed()) {
                    gc.closePath();
                }
                gc.stroke();

                if (this.LOD > LOD.COMPACT && this.#pickedAxis !== -1) {
                    gc.strokeStyle = 'white';
                    gc.fillStyle = 'red';
                    gc.lineWidth = LINE_WIDTH;
                    gc.beginPath();
                    gc.arc(...this.#polylines[highlight[j]].points[this.#pickedAxis], Constants.PICK_TOLERANCE, 0, 2 * Math.PI);
                    gc.fill();
                    gc.stroke();
                }
            }
        }
        gc.restore();
    }

    drawLabels(gc) {
        if (this.LOD < LOD.COMPACT) return;
        Axis.drawLabels(gc, this.#axes, this.#bounds);
        Axis.drawAxesValues(gc, this.#axes, this.#bounds);
        this.drawTupleValues(gc);
    }

    drawTupleValues(gc) {
        this.#polylines.forEach((polyline, i) => {
            if (this.#data.entities[i].highlight.size > 0) {
                const extent = Math.min(this.#bounds.width, this.#bounds.height) / window.devicePixelRatio;
                Util.font(gc, 'sans-serif', 4, .04 * extent, 12, 12, '#909090');

                for (let j = 0; j < polyline.points.length; j++) {
                    gc.save();
                    gc.translate(...polyline.points[j]);
                    // TODO: Label position should be translated a bit in the direction
                    // of the axis to prevent overlap with the interaction handle
                    let value = this.#tuples[i][j];
                    gc.fillText(value, 0, 0);
                    gc.restore();
                }
            }
        });
    }

    draw(gc) {
        this.drawAxes(gc);
        this.drawPolylines(gc);
        this.drawLabels(gc);
    }

    bounds() {
        return this.#_bounds;
    }
}