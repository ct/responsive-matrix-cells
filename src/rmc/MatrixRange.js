import {Rectangle} from "../base/Rectangle.js";
import {Graph} from "../base/Graph.js";

export class MatrixRange {
    constructor(matrix, from, to = from) {
        this.matrix = matrix;
        this.from = {col: from[0], row: from[1]};
        this.to = {col: to[0], row: to[1]};
    }

    checkRange() {
        for (let k of ['col', 'row']) {
            if (this.from[k] === -1 || this.to[k] === -1) return false;
            if (this.from[k] > this.to[k]) { // Swap if from > to
                let h = this.to[k];
                this.to[k] = this.from[k];
                this.from[k] = h;
            }
        }
        return true;
    }

    shadowRange() {
        return new MatrixRange(this.matrix, [this.from.row, this.from.col], [this.to.row, this.to.col]);
    }

    reserve(factor = 2) {
        // Scale up cols and rows in the range by factor
        this.foreachColumn(col => {
            let w = col._width.target[0] * ((col.locked) ? 1 : factor);
            col._width.animate([w]);
            col.locked++;
        });
        this.foreachRow(row => {
            let h = row._height.target[0] * ((row.locked) ? 1 : factor);
            row._height.animate([h]);
            row.locked++;
        });
    }

    release() {
        this.foreachColumn(col => col.locked = (col.locked > 0) ? col.locked - 1 : 0); // Release rows and columns
        this.foreachRow(row => row.locked = (row.locked > 0) ? row.locked - 1 : 0);
    }

    nodes() {
        let n = new Set();
        for (let i = this.from.col; i <= this.to.col; i++) {
            n.add(this.matrix.column(i).node);
        }
        for (let i = this.from.row; i <= this.to.row; i++) {
            n.add(this.matrix.row(i).node);
        }

        n = [...n]; // Make array from set
        n.subgraph = () => this.matrix.graph.subgraph(Graph.NODE, n); // Attach subgraph function

        return n;
    }

    edges() {
        let e = [];
        this.foreachCell((i, j) => {
            let edge = this.matrix.edge(i, j);
            if (edge != null) e.push(edge);
        });

        e.subgraph = () => this.matrix.graph.subgraph(Graph.EDGE, e); // Attach subgraph function

        return e;
    }

    entities(entitytype) {
        return (entitytype === Graph.NODE) ? this.nodes() : this.edges();
    }

    rect() {
        let x = this.matrix.column(this.from.col).x;
        let y = this.matrix.row(this.from.row).y;
        let w = this.matrix.column(this.to.col).x + this.matrix.column(this.to.col).width - x;
        let h = this.matrix.row(this.to.row).y + this.matrix.row(this.to.row).height - y;
        return new Rectangle(x, y, w, h);
    }

    foreachColumn(callback) {
        for (let i = this.from.col; i <= this.to.col; i++) {
            callback(this.matrix.column(i));
        }
    };

    foreachRow(callback) {
        for (let j = this.from.row; j <= this.to.row; j++) {
            callback(this.matrix.row(j));
        }
    };

    foreachCell(callback) {
        for (let i = this.from.col; i <= this.to.col; i++) {
            for (let j = this.from.row; j <= this.to.row; j++) {
                callback(i, j);
            }
        }
    }
}