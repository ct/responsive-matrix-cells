import chroma from "chroma-js";
import {Animatable} from "../base/Animatable.js";
import {Constants} from "../base/Constants.js";
import {ColorBrewer} from "../base/ColorBrewer.js";
import {LOD} from "./LOD";
import {Util} from "../base/Util";
import {Rectangle} from "../base/Rectangle";
import {D3ForceLayout} from "../base/D3ForceLayout";

export class NodeLink {

    #viewport;

    #dots;
    #links;

    #pickedDot = -1;

    #doColor = true;
    #doSize = true;
    #doWidth = true;
    #doLayout = true;

    #encodingValueNode;
    #encodingValueEdge;

    #layout;

    #backcolor;
    #bounds;

    constructor(graph, viewport, refresh, backcolor = () => '#00000000', cellEntity = 0) {

        this.#viewport = viewport;
        this.#backcolor = backcolor;
        this.#bounds = new Rectangle();

        this.#dots = graph.nodes.map(node => ({
                node: node,
                neighbors: [...node.neighbors], // Copy list of neighbor indices
                x: 0,
                y: 0,
                worldPosition: new Animatable([0, 0]),
                viewPosition: [],
                radius: 0,
                color: 'rgb(0, 0, 0)',
            })
        );

        this.#links = graph.edges.map(edge => ({
                edge: edge,
                source: this.#dots[edge.src],
                target: this.#dots[edge.dst],
                width: 0,
            })
        );

        this.#encodingValueNode = node => (graph.max.node.degree != graph.min.node.degree) ? node.degree / (graph.max.node.degree - graph.min.node.degree) : 0;
        this.#encodingValueEdge = edge => (graph.max.edge.weight != graph.min.edge.weight) ? edge.weight / (graph.max.edge.weight - graph.min.edge.weight) : 0;

        this.visualEncoding();

        this.#layout = new D3ForceLayout(this);
    }

    get dots() {
        return this.#dots;
    }

    get links() {
        return this.#links;
    }

    get doColor() {
        return this.#doColor;
    }

    set doColor(value) {
        this.#doColor = value;
    }

    get doSize() {
        return this.#doSize;
    }

    set doSize(value) {
        this.#doSize = value;
    }

    get doWidth() {
        return this.#doWidth;
    }

    set doWidth(value) {
        this.#doWidth = value;
    }

    get doLayout() {
        return this.#doLayout;
    }

    set doLayout(value) {
        this.#doLayout = value;
    }

    get layout() {
        return this.#layout;
    }

    visualEncoding() {
        const MIN_RADIUS = 5 * window.devicePixelRatio;
        const MAX_RADIUS = 50 * window.devicePixelRatio;
        const FIX_RADIUS = 7 * window.devicePixelRatio;

        const MIN_WIDTH = 0.2 * window.devicePixelRatio;
        const MAX_WIDTH = 5 * window.devicePixelRatio;
        const FIX_WIDTH = 1 * window.devicePixelRatio;

        const minArea = MIN_RADIUS * MIN_RADIUS * Math.PI / 4;
        const maxArea = MAX_RADIUS * MAX_RADIUS * Math.PI / 4;

        this.#dots.forEach(dot => {
            const t = this.#encodingValueNode(dot.node);
            dot.color = (this.doColor) ? chroma.scale(ColorBrewer["YlGn"][5]).mode("lab")(t).hex() : 'rgb(190, 190, 190)';

            const area = minArea * (1 - t) + maxArea * t;
            dot.radius = (this.doSize) ? Math.sqrt(area / Math.PI) : FIX_RADIUS;

        });

        this.#links.forEach(link => {
            const t = this.#encodingValueEdge(link.edge);
            link.width = (this.doWidth) ? MIN_WIDTH * (1 - t) + MAX_WIDTH * t : FIX_WIDTH;
        });
    }

    pick(evt) {
        let dx;
        let dy;

        if (this.#pickedDot !== -1) {
            this.#dots[this.#pickedDot].node.highlight.delete(this);
            this.#pickedDot = -1;
        }

        let n = this.#dots.length;
        for (let i = 0; i < n && this.#pickedDot === -1; i++) { // Picking order should be inverse of drawing order
            dx = this.#dots[i].viewPosition[0] - evt.screenCoords[0];
            dy = this.#dots[i].viewPosition[1] - evt.screenCoords[1];
            this.#pickedDot = (dx * dx + dy * dy < this.#dots[i].radius * this.#dots[i].radius) ? i : -1;
        }

        if (this.#pickedDot !== -1) {
            this.#dots[this.#pickedDot].node.highlight.add(this);
        }

        return (this.#pickedDot !== -1); // Return true if node was picked, otherwise return false
    }

    onmousedown(evt) {
        if (!this.dragging) {
            if (evt.button === 0 && this.#pickedDot !== -1) {
                this.dragging = true;
                return true; // Event consumed
            }
        }
        return false; // Event not consumed
    }

    onmouseup(evt) {
        if (this.dragging) {
            if (evt.button === 0) {
                if (!evt.shiftKey) this.layout.unfixNode(this.#pickedDot);
                delete this.dragging;

                return true; // Event consumed
            }
        }
        return false;
    }

    onmousemove(evt) {
        if (this.dragging) {
            this.layout.fixNode(this.#pickedDot, evt.worldCoords[0], evt.worldCoords[1]);
        }
        return true; // Event consumed
    }

    onclose() {
        this.#dots.forEach(dot => dot.node.highlight.delete(this));
    }

    update(time) {
        let needUpdate = false;

        if (this.doLayout) {
            needUpdate = this.layout.update(time);
        }

        // Update positions according to lens and radar transformations
        let n = this.#dots.length;
        while (n--) {
            this.#dots[n].worldPosition.animate([this.#dots[n].x, this.#dots[n].y]);
            needUpdate = this.#dots[n].worldPosition.update(time) || needUpdate;
        }

        this.project();

        this.#bounds.set(...this.#viewport.project(this.bounds()));
        this.LOD = LOD.lod(Math.min(this.#bounds.width, this.#bounds.height));

        return needUpdate;
    }

    project() {
        let n = this.#dots.length;
        let p;
        while (n--) {
            p = this.#viewport.project(this.#dots[n].worldPosition.value);
            this.#dots[n].viewPosition[0] = p[0];
            this.#dots[n].viewPosition[1] = p[1];
        }
    }

    drawLinks(gc) {
        const contrastcolor = Util.contrastColor(this.#backcolor());

        gc.strokeStyle = (this.LOD > LOD.MINIATURE) ? '#777' : contrastcolor;
        let e = this.links.length;
        while (e--) {
            gc.beginPath();
            gc.moveTo(...this.links[e].source.viewPosition);
            gc.lineTo(...this.links[e].target.viewPosition);
            gc.lineWidth = (this.LOD > LOD.COMPACT) ? this.links[e].width : 0.5 * window.devicePixelRatio;
            gc.stroke();
        }
    }

    drawDots(gc) {
        if (this.LOD === LOD.PIXEL) return;
        const contrastcolor = Util.contrastColor(this.#backcolor());
        let scale = 1.0 / (2.0 * (4 - this.LOD));

        gc.lineWidth = 1 * window.devicePixelRatio;
        let i = this.dots.length;
        while (i--) {
            let n = this.dots[i];
            gc.beginPath();
            gc.strokeStyle = (n.node.highlight.size > 0) ? 'red' : '#777';
            gc.fillStyle = (this.LOD > LOD.MINIATURE) ? n.color : contrastcolor;
            gc.arc(...n.viewPosition, n.radius * scale, 0, 2 * Math.PI);
            gc.fill();
            if (this.LOD > LOD.MINIATURE) gc.stroke();
        }
    }

    draw(gc, grid) {
        this.visualEncoding(); // Re-run visual encoding just in case the device pixel ratio has changed
        this.drawLinks(gc); // Draw all edges
        this.drawDots(gc); // Draw all nodes
    }

    bounds() {
        let n = this.#dots.length;

        if (n === 0) return [-Constants.LAYOUT_EXTENT / 2, -Constants.LAYOUT_EXTENT / 2, Constants.LAYOUT_EXTENT, Constants.LAYOUT_EXTENT];

        let minX = this.#dots[0].x;
        let minY = this.#dots[0].y;
        let maxX = this.#dots[0].x;
        let maxY = this.#dots[0].y;
        while (n--) {
            if (this.#dots[n].x < minX) {
                minX = this.#dots[n].x;
            } else if (this.#dots[n].x > maxX) {
                maxX = this.#dots[n].x;
            }
            if (this.#dots[n].y < minY) {
                minY = this.#dots[n].y;
            } else if (this.#dots[n].y > maxY) {
                maxY = this.#dots[n].y;
            }
        }

        if (minX === maxX) {
            minX -= Constants.LAYOUT_EXTENT / 2;
            maxX += Constants.LAYOUT_EXTENT / 2;
        }
        if (minY === maxY) {
            minY -= Constants.LAYOUT_EXTENT / 2;
            maxY += Constants.LAYOUT_EXTENT / 2;
        }

        return [minX, minY, maxX - minX, maxY - minY];
    }
}
