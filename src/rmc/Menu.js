import {Graph} from "../base/Graph";
import {Constants} from "../base/Constants";
import {MetaCell, UnitCell} from "./ResponsiveCell";
import {SoccerGraph} from "../base/Graph";

export class Menu {

    static attrs = {
        [Graph.NODE]: [...Constants.DEFAULT_NODE_ATTRIBUTES],
        [Graph.EDGE]: [...Constants.DEFAULT_EDGE_ATTRIBUTES],
    }

    static select = {
        [Graph.NODE]: Object.entries(SoccerGraph.LABELS[Graph.NODE]).map(entry => ({name: entry[1], value: entry[0]})),
        [Graph.EDGE]: Object.entries(SoccerGraph.LABELS[Graph.EDGE]).map(entry => ({name: entry[1], value: entry[0]})),
    }

    static inited = false;
    static matrix = undefined;
    static cell = undefined;
    static cellindex = -1;

    constructor(refresh) {
        this.refresh = refresh;

        if (Menu.inited) return;

        Menu.inited = true;

        $('#floating-menu').accordion({
            exclusive: false // Allow multiple fields to be open at the same time (needs more space)
        });

        // Set up botton for closing the menu
        $('#floating-menu-close-button').click((evt) => {
            this.hide();
        });

        $('#floating-menu-trash-button').click((evt) => {
            if (!Menu.matrix) return;
            Menu.matrix.removeRMCell(Menu.cellindex);
            this.refresh();
        });

        // Set up checkbox toggle for switching cell type
        $('#cell-type-toggle').checkbox({
            onChange: () => {
                if (!Menu.matrix) return;

                const newCellType = document.getElementById('cell-type-toggle').querySelector('input').checked ? MetaCell : UnitCell;

                Menu.matrix.changeRMCellType(Menu.cellindex, newCellType);
                Menu.cell = Menu.matrix.getRMCell(Menu.cellindex); // Update cell reference in menu

                this.update();
                this.refresh();
            }
        });

        // Set up checkbox toggle for switching entity type
        $('#entity-type-toggle').checkbox({
            onChange: () => {
                if (!Menu.matrix) return;

                const newEntityType = document.getElementById('entity-type-toggle').querySelector('input').checked ? Graph.EDGE : Graph.NODE;

                Menu.matrix.changeRMCellEntityType(Menu.cellindex, newEntityType);
                Menu.cell = Menu.matrix.getRMCell(Menu.cellindex); // Update cell reference in menu

                this.update();
                this.refresh();
            }
        });

        $('.vis.button').click((evt) => {
            if (!Menu.matrix) return;

            switch (evt.currentTarget.attributes['data-vis'].value) {
                case 'bar-v':
                    Menu.cell.activeTechnique = 0;
                    Menu.cell.activeLayout = 0;
                    break;
                case 'bar-h':
                    Menu.cell.activeTechnique = 0;
                    Menu.cell.activeLayout = 1;
                    break;
                case 'axis-v':
                    Menu.cell.activeTechnique = 1;
                    Menu.cell.activeLayout = 0;
                    break;
                case 'axis-h':
                    Menu.cell.activeTechnique = 1;
                    Menu.cell.activeLayout = 1;
                    break;
                case 'axis-r':
                    Menu.cell.activeTechnique = 1;
                    Menu.cell.activeLayout = 2;
                    break;
                case 'node-link':
                    Menu.cell.activeTechnique = 2;
                    Menu.cell.activeLayout = -1;
                    break;
            }
            this.update();
            this.refresh();
        });

        $('#visualization-attributes-selection').dropdown({
            maxSelections: 8,
            onChange: (value, text, $choice) => {
                if (!Menu.matrix) return;

                let attrs = $('#visualization-attributes-selection').val();

                Menu.matrix.changeRMCellAttributes(Menu.cellindex, attrs);
                this.refresh();
            }
        });

        $('#visualization-attributes-for-similarity').click((evt) => {
            let attrs = $('#visualization-attributes-selection').val();
            $('#similarity-attributes-selection').dropdown('set exactly', attrs);
        });

    }

    show(at, matrix, cellindex) {
        document.getElementById('floating-menu').style.left = at[0] + 'px';
        document.getElementById('floating-menu').style.top = at[1] + 'px';
        document.getElementById('floating-menu').classList.add('open');


        Menu.matrix = matrix;
        Menu.cellindex = cellindex;
        Menu.cell = matrix.getRMCell(Menu.cellindex);
        this.update();
    }

    hide() {
        Menu.matrix = undefined;
        Menu.cell = undefined;
        Menu.cellindex = -1;

        document.getElementById('floating-menu').classList.remove('open');
    }

    update(cell) { // Internal calls may omit cell, external calls can provide an updated cell to be set to the menu

        Menu.cell = cell || Menu.cell;
        if (!Menu.cell) return;

        const m = Menu.matrix;
        Menu.matrix = undefined; // Pause onchange handler of dropdown

        // Set the content of the menu so as to reflect the state of the cell

        $('#cell-type-toggle').checkbox('set ' + (Menu.cell instanceof MetaCell ? 'checked' : 'unchecked'));
        $('#entity-type-toggle').checkbox('set ' + (Menu.cell.entitytype === Graph.EDGE ? 'checked' : 'unchecked'));

        let vis;
        if (Menu.cell.activeTechnique === 0) {
            vis = Menu.cell.activeLayout === 0 ? 'bar-v' : 'bar-h'
        } else if (Menu.cell.activeTechnique === 1) {
            vis = Menu.cell.activeLayout === 0 ? 'axis-v' : Menu.cell.activeLayout === 1 ? 'axis-h' : 'axis-r';
        } else if (Menu.cell.activeTechnique === 2) {
            vis = 'node-link';
        }

        $('.vis.button').removeClass('active');
        $(`.vis.button[data-vis=${vis}]`).addClass('active');

        if (Menu.cell instanceof UnitCell) {
            $('[data-vis=node-link]').addClass('disabled'); // No node-link for multi cells
        } else {
            $('[data-vis=node-link]').removeClass('disabled');
        }

        // Populate attribute selection dropdowns with options based on data attributes
        const options = {values: Menu.select[Menu.cell.entitytype]};

        const $vas = $('#visualization-attributes-selection');
        $vas.empty();
        options.values.forEach(entry => {
            let {name, value} = entry;
            $vas.append(`<option value="${value}">${name}</option>`);
        });

        $vas.dropdown('clear');
        $vas.dropdown('setup menu', options);
        $vas.dropdown('set exactly', Menu.cell.attrs);


        if (Menu.cell.entitytype === Graph.EDGE) {
            $('#visualization-attributes-for-similarity').addClass('disabled');
        } else {
            $('#visualization-attributes-for-similarity').removeClass('disabled');
        }

        Menu.matrix = m; // Re-enable event handling
    }
}
