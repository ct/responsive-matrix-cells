export class AxesLayout {

    static HORIZONTAL_PCP = 0;
    static VERTICAL_PCP = 1;
    static STAR = 2;
    static ARRANGEMENTS = ['horizontalPCP', 'verticalPCP', 'star'];
    static CLOSED = [false, false, true];

    #done;
    #arrangement;
    #axesplot;

    constructor(axesplot, arrangement) {
        this.#done = false;
        this.#arrangement = arrangement;
        this.#axesplot = axesplot;
    }

    horizontalPCP() {
        const b = this.#axesplot.bounds();
        if (this.#axesplot.axes.length === 1) {
            let axis = this.#axesplot.axes[0];
            axis._start.animate([b.left + b.width / 2, b.bottom]);
            axis._end.animate([b.left + b.width / 2, b.top]);
        } else {
            const gap = b.width / (this.#axesplot.axes.length - 1);
            this.#axesplot.axes.forEach((axis, i) => {
                axis._start.animate([b.left + i * gap, b.bottom]);
                axis._end.animate([b.left + i * gap, b.top]);
            });
        }
    }

    verticalPCP() {
        const b = this.#axesplot.bounds();
        if (this.#axesplot.axes.length === 1) {
            let axis = this.#axesplot.axes[0];
            axis._start.animate([b.left, b.top + b.height / 2]);
            axis._end.animate([b.right, b.top + b.height / 2]);
        } else {
            const gap = b.height / (this.#axesplot.axes.length - 1);
            this.#axesplot.axes.forEach((axis, i) => {
                axis._start.animate([b.left, b.top + i * gap]);
                axis._end.animate([b.right, b.top + i * gap]);
            });
        }
    }

    star() {
        const b = this.#axesplot.bounds();
        const radius = .5 * Math.min(b.width, b.height);
        const innerRadius = 0.1 * radius;
        const cx = b.x + b.width / 2;
        const cy = b.y + b.height / 2;
        const gap = 2 * Math.PI / this.#axesplot.axes.length;
        this.#axesplot.axes.forEach((axis, i) => {
            // axis._start.animate([cx, cy]);
            axis._start.animate([innerRadius * Math.cos(i * gap), innerRadius * Math.sin(i * gap)]);
            axis._end.animate([radius * Math.cos(i * gap), radius * Math.sin(i * gap)]);
        });
    }

    closed() {
        return AxesLayout.CLOSED[this.#arrangement];
    }

    update(time) {
        if (this.done) return false;
        this[AxesLayout.ARRANGEMENTS[this.#arrangement]]();
        this.done = true;
        return !this.done;
    };

    invalidate() {
        this.done = false;
    };
}