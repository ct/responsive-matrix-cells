import {BarChart} from "./BarChart.js";
import {AxesPlot} from "./AxesPlot.js";
import {NodeLink} from "./NodeLink.js";
import {Matrix} from "./Matrix.js";
import {Viewport} from "../base/Viewport.js";
import {Util} from "../base/Util.js";
import {LOD} from "./LOD.js";
import {MatrixRange} from "./MatrixRange.js";

class ResponsiveCell {

    matrix;
    entitytype;
    attrs;
    range;
    refresh;

    constructor(matrix, entitytype, attrs, range, refresh) {
        this.matrix = matrix;
        this.entitytype = entitytype;
        this.attrs = attrs;
        this.range = range;
        this.refresh = refresh;
    }
}

export class MetaCell extends ResponsiveCell {

    #window;
    #viewport;
    #subgraph;
    #technique;
    #techniques;
    #activeTechnique;

    constructor(matrix, entitytype, attrs, range, refresh, backcolor = () => '#FFFFFFFF', ctors = [BarChart, AxesPlot, NodeLink]) {
        super(matrix, entitytype, attrs, range, refresh);

        this.backcolor = backcolor;

        // The window where the responsive cell is to be rendered depends on the extent of the underlying rows and columns
        this.#window = {
            get w() {
                return matrix.column(range.to.col).x + matrix.column(range.to.col).width - matrix.column(range.from.col).x; // Width w/o border: Last column's x + width (right) minus first column's x
            },
            get h() {
                return matrix.row(range.to.row).y + matrix.row(range.to.row).height - matrix.row(range.from.row).y; // Height w/o border: Last row's y + height (bottom) minus first rows's y
            },
            get border() {
                return Math.min(4, .1 * Math.min(this.w, this.h)); // Add some pixel border
            },
            get x() {
                return matrix.column(range.from.col).x + this.border; // X w/ border
            },
            get y() {
                return matrix.row(range.from.row).y + this.border; // Y w/ border
            },
            get width() {
                let col = matrix.column(range.to.col);
                return col.x + col.width - this.x - this.border; // Width w/ border
            },
            get height() {
                let row = matrix.row(range.to.row);
                return row.y + row.height - this.y - this.border;  // Height w/ border
            }
        };

        // The local viewport based on the dynamic window
        this.#viewport = new Viewport(this.#window);
        this.#subgraph = range.entities(entitytype).subgraph();

        this.#techniques = ctors.map(t => new t(this.#subgraph, this.#viewport, refresh, backcolor, entitytype));
        this.#technique = this.#techniques[0];
        this.activeTechnique = 0;

        // Create handlers that redirect to the handlers of the active technique
        Matrix.HANDLERS.forEach(handler => {
            this[handler] = (evt) => {
                const oldWorld = evt.worldCoords;
                if (evt.screenCoords) evt.worldCoords = this.#viewport.unproject(evt.screenCoords);
                const handled = this.#technique[handler] && this.#technique[handler](evt);
                evt.worldCoords = oldWorld;
                return handled;
            };
        });

        this.changeAttrs(attrs);
    }

    get activeLayout() {
        return this.#technique.activeLayout;
    }

    set activeLayout(l) {
        if (this.activeLayout !== undefined)
            this.#technique.activeLayout = l;
    }

    get activeTechnique() {
        return this.#activeTechnique;
    }

    set activeTechnique(t) {
        this.#technique.onclose();
        this.#activeTechnique = Util.keepModulo(t, this.#techniques.length);
        this.#technique = this.#techniques[this.activeTechnique];
    }

    get viewbounds() {
        return [this.#window.x, this.#window.y, this.#window.width, this.#window.height];
    }

    changeAttrs(attrs) {
        this.attrs = attrs;
        let data = this.#subgraph.data(attrs, this.entitytype);
        this.#techniques.forEach(t => t.setdata && t.setdata(data)); // Set data only where techniques has setdata
    }

    onclose() {
        this.#technique.onclose();
    }

    update(time) {
        // TODO: Check automatic viewport adjustment
        //  Adjusting the viewport on each render step is only necessary if the techniques bounds vary. This
        //  is the case for example for the NodeLink layout. However, when the techniques bounds change as a result
        //  of interaction (e.g., a node is dragged outside the current bounds), adjusting the viewport on the new
        //  bounds can lead to interaction and viewport adjustment to get out of control, i.e., infinity expansion.
        //  Alternatively, the viewport could be set only once when the technique is created. This would be absolutely
        //  fine for techniques with fixed bounds.
        let bounds = [...this.#technique.bounds()]; // Get bounds of the technique

        // if (true) { // Should aspect ratio be maintained?
        //     if (bounds[2] > bounds[3]) {
        //         bounds[2] *= this.#window.width / this.#window.height;
        //     }
        //     else {
        //         bounds[3] *= this.#window.height / this.#window.width;
        //     }
        // }

        // Allow for some border space around technique bounds (for axes and arrows)
        let factor = 0.05;
        bounds[0] -= factor * bounds[2];
        bounds[1] -= factor * bounds[3];
        bounds[2] += 2 * factor * bounds[2];
        bounds[3] += 2 * factor * bounds[3];

        this.#viewport.animatable.step(bounds); // Center viewport on technique

        let needUpdate = this.#technique.update(time);

        return needUpdate;
    };

    render(gc) {
        gc.save();
        gc.beginPath();
        gc.rect(...this.viewbounds);
        gc.clip();
        if (this.#technique.LOD < LOD.COMPACT) {
            gc.fillStyle = this.backcolor && this.backcolor();
            gc.fillRect(...this.viewbounds);
        } else {
            gc.clearRect(...this.viewbounds);
        }
        this.#technique.draw(gc);
        gc.restore();
    };
}


export class UnitCell extends ResponsiveCell {

    #cells = [];

    constructor(matrix, entitytype, attrs, range, refresh) {
        super(matrix, entitytype, attrs, range, refresh);

        // Create separate responsive cells for each cell in the multi cell
        for (let i = range.from.col; i <= range.to.col; i++) {
            for (let j = range.from.row; j <= range.to.row; j++) {
                this.#cells.push(
                    new MetaCell(
                        matrix,
                        entitytype,
                        attrs,
                        new MatrixRange(matrix, [i, j], [i, j]),
                        refresh,
                        () => matrix.cellColor(i, j),
                        [BarChart, AxesPlot]) // MultiCells only offer BarChart and AxesPlot
                );
            }
        }

        // Create handlers that redirect to the responsive cell handlers
        Matrix.HANDLERS.forEach(handler => {
            this[handler] = (evt) => {
                let handled = false;
                this.#cells.forEach(cell => {
                    handled = cell[handler](evt) || handled;
                });
                return handled;
            };
        });
    }

    get activeLayout() {
        return this.#cells[0].activeLayout;
    }

    set activeLayout(l) {
        if (this.activeLayout !== undefined)
            this.#cells.forEach(cell => {
                cell.activeLayout = l;
            })
    }

    get activeTechnique() {
        return this.#cells[0].activeTechnique;
    }

    set activeTechnique(t) {
        this.#cells.forEach(cell => {
            cell.activeTechnique = t;
        });
    }

    changeAttrs(attrs) {
        this.attrs = attrs;
        this.#cells.forEach(cell => {
            cell.changeAttrs(attrs);
        })
    }

    onclose() {
        this.#cells.forEach(cell => {
            cell.onclose();
        });
    }

    update(time) {
        let needUpdate = false;
        this.#cells.forEach(cell => {
            needUpdate = cell.update(time) || needUpdate;
        });
        return needUpdate;
    };

    render(gc) {
        this.#cells.forEach(cell => {
            cell.render(gc);
        });
    };
}
