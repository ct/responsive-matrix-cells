import {Animatable} from "../base/Animatable.js";
import {Util} from "../base/Util.js";
import {LOD} from "./LOD.js";

export class Axis {

    #min;
    #max;

    constructor(attr, label, min, max) {
        this.attribute = attr;
        this.label = label;
        this.#min = new Animatable([min]);
        this.#max = new Animatable([max]);
        this.minmax(min, max);

        this._start = new Animatable([0, 0]);
        this._end = new Animatable([0, 0]);
        this.start = [0, 0];
        this.end = [0, 0];
    }

    static drawAxes(gc, axes, backcolor, bounds, lod, pickedAxis) {
        const contrastcolor = Util.contrastColor(backcolor());

        gc.save();
        gc.lineWidth = 1;
        gc.strokeStyle = (lod > LOD.MINIATURE) ? '#777' : contrastcolor;
        gc.beginPath();
        axes.forEach(axis => {
            gc.moveTo(...axis.start);
            gc.lineTo(...axis.end);
        });
        gc.stroke();

        if (lod > LOD.MINIATURE) {
            gc.fillStyle = '#777';
            const arrowsize = Math.min(15 * window.devicePixelRatio, .015 * Math.min(bounds.width, bounds.height));
            axes.forEach(axis => {
                gc.save();
                gc.translate(...axis.end);
                gc.rotate(Math.PI / 2 + Util.angle(axis.start, axis.end));
                gc.beginPath(); // Fill arrowhead
                gc.moveTo(-arrowsize / 2, 0);
                gc.lineTo(arrowsize / 2, 0);
                gc.lineTo(0, -arrowsize);
                gc.closePath();
                gc.fill();
                gc.restore();
            });
        }

        if (lod > LOD.MINIATURE && pickedAxis !== -1) {
            let axis = axes[pickedAxis];
            gc.lineWidth = 2;
            gc.strokeStyle = '#777';
            gc.beginPath();
            gc.moveTo(...axis.start);
            gc.lineTo(...axis.end);
            gc.stroke();
        }
        gc.restore();
    }

    static drawLabels(gc, axes, bounds) {
        const extent = Math.min(bounds.width, bounds.height) / window.devicePixelRatio;
        Util.font(gc, 'sans-serif', 4, .05 * extent, 14, 10, '#909090');

        let x, y, a;

        axes.forEach(axis => {
            gc.save();
            a = Util.angle(axis.start, axis.end);
            y = 2;
            if (Math.abs(a) > 0.5 * Math.PI + 0.1) {
                a += Math.PI;
                x = 0;
            } else {
                x = -gc.measureText(axis.label).width;
            }
            gc.translate(...axis.end);
            gc.rotate(a);
            gc.textBaseline = 'top';
            gc.fillText(axis.label, x, y, axis.length());
            gc.restore();
        });
    }

    //min and max axes labels
    static drawAxesValues(gc, axes, bounds) {
        const extent = Math.min(bounds.width, bounds.height) / window.devicePixelRatio;
        let size = Util.font(gc, 'sans-serif', 4, .04 * extent, 12, 8, '#909090');

        let x_max, y_max, x_min, y_min, a, min, max;

        let lastMin;
        axes.forEach(axis => {
            a = Util.angle(axis.start, axis.end);
            min = axis.#min.value[0];
            max = Number.isInteger(axis.#max.value[0]) ? axis.#max.value[0] : axis.#max.value[0].toFixed(3);

            if (Math.abs(a) > 0.5 * Math.PI + 0.1) {
                a += Math.PI;
                x_max = 0;
                x_min = -gc.measureText(min).width;
            } else {
                x_min = 0;
                y_min = -2 - size;
                x_max = -gc.measureText(max).width;
                y_max = -2 - size;
            }
            if (lastMin !== min) {
                gc.save();
                gc.translate(...axis.start);
                gc.rotate(a);
                gc.textBaseline = 'top';
                gc.fillText(min, x_min, y_min);
                gc.restore();
            }
            lastMin = min;

            gc.save();
            gc.translate(...axis.end);
            gc.rotate(a);
            gc.textBaseline = 'top';
            gc.fillText(max, x_max, y_max);
            gc.restore();
        });

    }

    minmax(min, max) {
        this.#min.animate([min]);
        this.#max.animate([max]);
    }

    mapParametric(value) {
        const min = this.#min.value[0];
        const range = this.#max.value[0] - min;
        const t = (value - min) / range;
        return t;
    }

    map(value) {
        const t = this.mapParametric(value);
        const s = this._start.value;
        const e = this._end.value;
        const dx = e[0] - s[0];
        const dy = e[1] - s[1];
        return [s[0] + t * dx, s[1] + t * dy];
    }

    unmap(p) {
        const min = this.#min.value[0];
        const range = this.#max.value[0] - min;
        const t = this.projectParametric(p, false);
        const value = min + t * range;
        return value;
    }

    update(time) {
        let needUpdate = false;
        needUpdate = this._start.update(time) || needUpdate;
        needUpdate = this._end.update(time) || needUpdate;
        needUpdate = this.#min.update(time) || needUpdate;
        needUpdate = this.#max.update(time) || needUpdate;
        return needUpdate;
    }

    projectParametric(p, clamp = true) {
        return Util.projectPointToLineParametric(p, this.start, this.end, clamp);
    }

    project(p, clamp = true) {
        return Util.projectPointToLine(p, this.start, this.end, clamp);
    }

    distanceSq(p) {
        return Util.distanceSq(p, this.project(p));
    }

    distance(p) {
        return Util.distance(p, this.project(p));
    }

    length() {
        return Util.distanceSq(this.start, this.end);
    }
}

export class EditInput {

    static inited = false;
    static vis = undefined;
    static tuple = undefined;
    static value = undefined;

    constructor(refresh) {
        this.refresh = refresh;
        if (EditInput.inited) return;

        EditInput.inited = true;

        $('#edit-menu').append($('<input type="text" id="edit-input" placeholder="Value...">'));

        $('#edit-input').keyup((evt) => {
            if (evt.key === "Enter") {
                EditInput.value = Number($('#edit-input').val());
                EditInput.vis.editTuple(EditInput.tuple, EditInput.attr, EditInput.value);
                this.hide();
                this.refresh();
            }
        });

    }

    show(at, vis, attr, tuple, value) {
        document.getElementById('edit-menu').style.left = at[0] + 'px';
        document.getElementById('edit-menu').style.top = at[1] + 'px';
        document.getElementById('edit-menu').classList.add('open');
        $('#edit-input').val(value);

        EditInput.vis = vis;
        EditInput.tuple = tuple;
        EditInput.attr = attr;
    }

    hide() {
        EditInput.vis = undefined;
        EditInput.tuple = undefined;
        EditInput.value = undefined;
        $('#edit-input').val(undefined);

        document.getElementById('edit-menu').classList.remove('open');
    }
}