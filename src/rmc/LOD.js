export class LOD {
    static PIXEL = 0;
    static MINIATURE = 1;
    static COMPACT = 2;
    static MEDIUM = 3;

    static lod(extent) {
        if (extent > 150 * window.devicePixelRatio) return LOD.MEDIUM;
        if (extent > 50 * window.devicePixelRatio) return LOD.COMPACT;
        if (extent > 10 * window.devicePixelRatio) return LOD.MINIATURE;
        return LOD.PIXEL;
    }
}