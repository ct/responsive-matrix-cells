import chroma from "chroma-js";
import {Rectangle} from "../base/Rectangle.js";
import {Constants} from "../base/Constants.js";
import {MatrixLayout} from "./MatrixLayout.js";
import {Animatable} from "../base/Animatable.js";
import {MetaCell, UnitCell} from "./ResponsiveCell.js";
import {Menu} from "./Menu.js";
import {Util} from "../base/Util.js";
import {MatrixRange} from "./MatrixRange.js";
import {Graph} from "../base/Graph.js";
import {Similarity} from "../base/Similarity.js";

export class Matrix {

    static HANDLERS = ["pick", "onclick", "ondblclick", "onwheel", "onmousedown", "onmouseup", "onmousemove", "onkeypress", "onkeydown"];

    #viewport;
    #refresh;

    #graph;

    #adjacency;
    #similarity;

    #columns;
    #columnOrder;
    #rows;
    #rowOrder;

    #pickedCol;
    #pickedRow;

    #_bounds;
    #bounds;

    #layout;

    #rmCells;
    #pickedRMCell;
    #pickedRMCorner;

    #similarityattributes; //Attributes used for similarity calculation
    #similarityColorScale = "RdYlGn";

    #edgeColorScale = "Blues";

    constructor(graph, viewport, refresh) {

        this.#viewport = viewport;
        this.#refresh = refresh;

        this.menu = new Menu(this.#refresh);

        const n = graph.nodes.length;

        this.#graph = graph;
        this.#adjacency = new Array(n);

        for (let i = 0; i < n; i++) {
            this.#adjacency[i] = new Array(n);
            this.#adjacency[i].node = graph.nodes[i];
            for (let j = 0; j < n; j++) {
                this.#adjacency[i][j] = {
                    edge: null, // Edge associated with the cell
                    color: 'white', // Color of the cell
                }
            }
        }

        // Basic visual encoding
        graph.edges.forEach(edge => {
            this.#adjacency[edge.src][edge.dst].edge = edge;
            this.#adjacency[edge.src][edge.dst].color = chroma.scale(this.#edgeColorScale).mode("lab")(edge.weight / graph.max.edge.weight).hex();

            this.#adjacency[edge.dst][edge.src].edge = edge;
            this.#adjacency[edge.dst][edge.src].color = this.#adjacency[edge.src][edge.dst].color;
        });

        this.encodeSimilarity(Constants.DEFAULT_NODE_ATTRIBUTES);

        // Initialize column and row orders
        this.#columnOrder = this.#adjacency.map((_, i) => i);
        this.#rowOrder = this.#adjacency.map((_, i) => i);

        // Variables starting w/ underscore _x, _height, _bounds are in world space
        // Corresponding variables w/o underscore x, height, bounds are in view space

        // Create 1-dim array to store column information
        this.#columns = this.#adjacency.map(_ => {
            return {
                node: _.node,
                _x: new Animatable([0]), // Position of column in world space
                _width: new Animatable([0]), // Width of column in world space
                x: 0, // ... in view space
                width: 0,
                locked: 0 // How many cells are locked on this column
            }
        });

        // Create 1-dim array to store row information
        this.#rows = this.#adjacency.map(_ => {
            return {
                node: _.node,
                _y: new Animatable([0]), // Position of row in world space
                _height: new Animatable([0]), // Height of row in world space
                y: 0, // ... in view space
                height: 0,
                locked: 0 // How many cells are locked on this row
            };
        });

        this.#_bounds = new Rectangle(-Constants.LAYOUT_EXTENT / 2, -Constants.LAYOUT_EXTENT / 2, Constants.LAYOUT_EXTENT, Constants.LAYOUT_EXTENT); // In world space
        this.#bounds = new Rectangle(); // In view space

        this.#pickedCol = -1;
        this.#pickedRow = -1;

        this.#layout = new MatrixLayout(this); // The matrix layout

        this.#rmCells = []; // Store for responsive cells
        this.#pickedRMCell = -1; // Which cell is under the cursor
        this.#pickedRMCorner = null; // Which cell corner is near the cursor

        // Prepend handler for each regular event to be handled
        Matrix.HANDLERS.forEach(handler => {
            let original = (this[handler]) ? this[handler].bind(this) : (evt => false); // Original handler or simple "false"
            this[handler] = evt => {
                if (this.#pickedRMCorner !== null) {
                    return original(evt); // Lets hear what the original event handler has to say
                } else if ((this.#pickedRMCell !== -1) && this.#rmCells[this.#pickedRMCell][handler](evt)) {
                    return true; // If responsive cell has dealt with the event, return true
                } else { // Otherwise
                    return original(evt); // Lets hear what the original event handler has to say
                }
            }
        }, this);

        //
        graph.register(Util.debounce((type, target, attr, old, value) => {
            if (type === Graph.NODE) {
                this.encodeSimilarity(this.#similarityattributes);
            } else if (type === Graph.EDGE) {
                this.encodeAdjacency();
            }
        }, 500));
    }

    get adjacency() {
        return this.#adjacency;
    }

    get graph() {
        return this.#graph;
    }

    get similarityColorScale() {
        return this.#similarityColorScale;
    }

    set similarityColorScale(scale) {
        this.#similarityColorScale = scale;
    }

    get edgeColorScale() {
        return this.#edgeColorScale;
    }

    set edgeColorScale(scale) {
        this.#edgeColorScale = scale;
    }

    encodeAdjacency() {
        this.#graph.edges.forEach(edge => {
            this.#adjacency[edge.src][edge.dst].edge = edge;
            this.#adjacency[edge.src][edge.dst].color = chroma.scale(this.#edgeColorScale).mode("lab")(edge.weight / this.#graph.max.edge.weight).hex();

            this.#adjacency[edge.dst][edge.src].edge = edge;
            this.#adjacency[edge.dst][edge.src].color = this.#adjacency[edge.src][edge.dst].color;
        });
    }

    encodeSimilarity(attrs, callback) {
        this.#similarityattributes = attrs;
        this.#similarity = Similarity.compute(this.graph, attrs);
        this.updateSimilarityColorEncoding();
        callback && callback();
        this.#refresh();
    }

    updateSimilarityColorEncoding() {
        const n = this.#similarity.length;
        for (let i = 0; i < n; i++) {
            for (let j = i; j < n; j++) {
                this.#similarity[i][j].color = chroma.scale(this.#similarityColorScale).mode("lab")(this.#similarity[i][j].value).hex();
                this.#similarity[j][i].color = this.#similarity[i][j].color;
            }
        }
    }

    changeEdgeWeightColorScale() {
        this.#graph.edges.forEach(edge => {
            this.#adjacency[edge.src][edge.dst].color = chroma.scale(this.#edgeColorScale).mode("lab")(edge.weight / this.#graph.max.edge.weight).hex();
            this.#adjacency[edge.dst][edge.src].color = this.#adjacency[edge.src][edge.dst].color;
        });
    }

    bounds() {
        return this.#_bounds;
    }

    column(i) {
        return this.#columns[this.#columnOrder[i]];
    }

    row(j) {
        return this.#rows[this.#rowOrder[j]];
    }

    edge(i, j) {
        return this.#adjacency[this.#columnOrder[i]][this.#rowOrder[j]].edge;
    }

    addRMCell(range, start, cellctor, factor) {
        let [col, row] = this.cellAt(start); // At which cell started the creation of the cell
        let entitytype = (col >= row) ? Graph.NODE : Graph.EDGE;

        range.reserve(factor ? factor : 2);
        this.#layout.invalidate();

        // By using Menu.attrs, the attributes of newly created cells will have the same attributes
        // as the cell adjusted lastly
        const newRMCell = new cellctor(this, entitytype, Menu.attrs[entitytype], range, this.#refresh);
        this.#rmCells.push(newRMCell);
        return newRMCell;
    }

    getRMCell(i) {
        return this.#rmCells[i];
    }

    changeRMCellType(i, cellctor) {
        let cell = this.#rmCells[i];
        if (cell instanceof cellctor) return;
        cell.onclose(); // Close old cell
        this.#rmCells[i] = new cellctor(this, cell.entitytype, cell.attrs, cell.range, this.#refresh);  // Create cell of new type in same place
    }

    // Toggle type of a selected cell
    toggleRMCellType() {
        let type = this.#rmCells[this.#pickedRMCell];
        type = (type instanceof MetaCell) ? UnitCell : MetaCell;
        this.changeRMCellType(this.#pickedRMCell, type);
        this.menu.update(this.#rmCells[this.#pickedRMCell]);
    }

    toggleRMCellEntityType() {
        let cell = this.#rmCells[this.#pickedRMCell];
        let entity = (cell.entitytype === Graph.NODE) ? Graph.EDGE : Graph.NODE;
        this.changeRMCellEntityType(this.#pickedRMCell, entity);
        this.menu.update(this.#rmCells[this.#pickedRMCell]);
    }

    changeRMCellEntityType(i, entitytype) {
        let cell = this.#rmCells[i];
        if (cell.entitytype === entitytype) return;

        cell.onclose(); // Close old cell
        cell.range.release();
        this.#layout.invalidate();
        this.#layout.propagate(); // Force recomputation of layout now to avoid excessive scaling when swapping cells

        cell.range.shadowRange().reserve();
        this.#rmCells[i] = new cell.constructor(this, entitytype, Menu.attrs[entitytype], cell.range.shadowRange(), this.#refresh); // Create new cell for the shadow range
        this.#rmCells[i].activeTechnique = cell.activeTechnique;
        this.#rmCells[i].activeLayout = cell.activeLayout;

        this.#layout.invalidate();
    }

    changeRMCellAttributes(i, attrs) {
        let cell = this.#rmCells[i];
        cell.changeAttrs(attrs);
    }

    changeSimRMCellAttributes(attrs) {
        for (let i = 0; i < this.#rmCells.length; i++) {
            if (this.#rmCells[i].entitytype === Graph.NODE) this.changeRMCellAttributes(i, attrs);
        }
    }

    removeRMCell(i) {
        let cell = this.#rmCells[i];
        cell.onclose();
        cell.range.release();
        this.#layout.invalidate();

        if (this.#pickedRMCell === i) this.#pickedRMCell = -1;
        this.#rmCells = this.#rmCells.filter(c => c !== cell);

        // Close local menu of cell
        if (Menu.cellindex === i) this.menu.hide();
    }

    rmCellAt(coordinates) {
        let [x, y] = coordinates;
        let n = this.#rmCells.length;
        let cell = -1;
        let corner = null;

        for (let i = n - 1; i >= 0; i--) { // Picking order must be inverse of drawing order
            const range = this.#rmCells[i].range;
            const rect = () => range.rect();
            const r = rect();
            if (cell === -1 && x >= r.left && x <= r.right && y >= r.top && y <= r.bottom) {
                cell = i;
            }

            const pts = [
                () => [rect().left, rect().top],
                () => [rect().right, rect().top],
                () => [rect().right, rect().bottom],
                () => [rect().left, rect().bottom],
            ];

            let pi = pts.length;
            while (corner == null && pi--) {
                let d = Util.distanceSq(coordinates, pts[pi]());
                if (d < Constants.PICK_TOLERANCE_SQ) {
                    cell = i;
                    corner = {
                        rmCell: i,
                        corner: pi,
                        point: pts[pi]
                    };
                }
            }
        }
        return [cell, corner];
    }

    cellBounds(i, j) {
        return [this.#columns[this.#columnOrder[i]].x, this.#rows[this.#rowOrder[j]].y, this.#columns[this.#columnOrder[i]].width, this.#rows[this.#rowOrder[j]].height];
    }

    cellBoundsNoTearing(i, j) {
        let x = this.#columns[this.#columnOrder[i]].x;
        let y = this.#rows[this.#rowOrder[j]].y;
        let next_x = (i < this.#columnOrder.length - 1) ? this.#columns[this.#columnOrder[i + 1]].x : this.#bounds[0] + this.#bounds[2];
        let next_y = (j < this.#rowOrder.length - 1) ? this.#rows[this.#rowOrder[j + 1]].y : this.#bounds[1] + this.#bounds[3];
        let w = next_x - x;
        let h = next_y - y;
        return [x, y, w, h];
    }

    cellAt(coordinates) {
        const cell = [-1, -1];
        const [x, y] = coordinates;
        const n = this.#columns.length;

        if (n === 0 || x < this.#columns[this.#columnOrder[0]].x || x > this.#columns[this.#columnOrder[n - 1]].x + this.#columns[this.#columnOrder[n - 1]].width ||
            y < this.#rows[this.#rowOrder[0]].y || y > this.#rows[this.#rowOrder[n - 1]].y + this.#rows[this.#rowOrder[n - 1]].height) {
            return cell; // Quickly return if x,y out of bounds
        }

        let i = 0;
        while (i < n && x > this.#columns[this.#columnOrder[i]].x) {
            i++;
        }
        if (n > 0 && x <= this.#columns[this.#columnOrder[n - 1]].x + this.#columns[this.#columnOrder[n - 1]].width) {
            cell[0] = i - 1;
        }

        const m = this.#rows.length;
        let j = 0;
        while (j < m && y > this.#rows[this.#rowOrder[j]].y) {
            j++;
        }
        if (m > 0 && y <= this.#rows[this.#rowOrder[m - 1]].y + this.#rows[this.#rowOrder[m - 1]].height) {
            cell[1] = j - 1;
        }
        return cell;
    }

    pick(evt) {
        // Pick new column, row, and responsive cell

        if (this.#pickedCol !== -1) {
            this.#adjacency[this.#columnOrder[this.#pickedCol]].node.highlight.delete(this);

            // delete edge highlights from matrix to cell
            this.#adjacency[this.#columnOrder[this.#pickedCol]].node.edges.forEach(
                edge => this.#graph.edges[edge].highlight.delete(this)
            );

            this.#pickedCol = -1;
        }
        if (this.#pickedRow !== -1) {
            this.#adjacency[this.#rowOrder[this.#pickedRow]].node.highlight.delete(this);

            // delete edge highlights from matrix to cell
            this.#adjacency[this.#rowOrder[this.#pickedRow]].node.edges.forEach(
                edge => this.#graph.edges[edge].highlight.delete(this)
            );

            this.#pickedRow = -1;
        }

        [this.#pickedRMCell, this.#pickedRMCorner] = this.rmCellAt(evt.screenCoords);

        if (this.#pickedRMCell === -1) {
            [this.#pickedCol, this.#pickedRow] = this.cellAt(evt.screenCoords);

            if (this.#pickedCol !== -1) {
                this.#adjacency[this.#columnOrder[this.#pickedCol]].node.highlight.add(this);

                // add edge highlights from matrix to cell
                this.#adjacency[this.#columnOrder[this.#pickedCol]].node.edges.forEach(
                    edge => this.#graph.edges[edge].highlight.add(this)
                );
            }
            if (this.#pickedRow !== -1) {
                this.#adjacency[this.#rowOrder[this.#pickedRow]].node.highlight.add(this);

                // add edge highlights from matrix to cell
                this.#adjacency[this.#rowOrder[this.#pickedRow]].node.edges.forEach(
                    edge => this.#graph.edges[edge].highlight.add(this)
                );
            }
        }

        return this.#pickedRMCell !== -1 || this.#pickedRMCorner != null || this.#pickedCol !== -1 || this.#pickedRow !== -1;
    }

    onmousedown(evt) {
        this.menu.hide();

        if (evt.button === 0 || evt.button === 1) {

            // The following if tests whether a middle click would have an effect on the matrix
            // If not, false is returned to indicate the click was not handled by this matrix, giving
            // a possible parent matrix the chance to handle the click
            // This is currently relevant because a middle click releases responsive cells
            if (evt.button === 1 && this.#pickedRMCell === -1 &&
                this.#pickedCol !== -1 && this.column(this.#pickedCol).locked === 0 &&
                this.#pickedRow !== -1 && this.row(this.#pickedRow).locked === 0) return false;

            // Click may be left or middle button
            this.click = {
                down: evt.screenCoords // Coordinates where drag started
            };

            if (evt.button === 0) {
                if (this.#pickedCol !== -1 && this.#pickedRow !== -1) {
                    this.drag = {
                        down: evt.screenCoords, // Coordinates where drag started
                        dragged: false, // Actually dragged after threshold has been reached ?
                        from: [this.#pickedCol, this.#pickedRow], // Cell where drag started
                        to: [this.#pickedCol, this.#pickedRow] // Cell where drag ended
                    };
                    return true; // Event consumed
                } else if (this.#pickedRMCorner !== null) {
                    let columns = [];
                    let rows = [];
                    const rmCell = this.#rmCells[this.#pickedRMCorner.rmCell];
                    rmCell.range.foreachColumn(col => columns.push(col._width.target[0]));
                    rmCell.range.foreachRow(row => rows.push(row._height.target[0]));

                    if (!evt.shiftKey) {
                        const prepareContext = (start, end) => {
                            // Store required information for context area around the cell, specifically the range object and the initial column/row sizes

                            let conObj = {range: new MatrixRange(this, start, end)};
                            let rangeOkay = conObj.range.checkRange();

                            // If range is outside of matrix, return null
                            if (!rangeOkay || conObj.range.from.col < 0 || conObj.range.from.row < 0 || conObj.range.to.col >= this.#columns.length || conObj.range.to.row >= this.#rows.length) {
                                return {range: null};
                            }

                            conObj.startSize = {cols: [], rows: []};
                            conObj.startRect = conObj.range.rect();
                            conObj.range.foreachColumn(col => conObj.startSize.cols.push(col._width.target[0]));
                            conObj.range.foreachRow(row => conObj.startSize.rows.push(row._height.target[0]));
                            conObj.range.reserve(1);
                            return conObj;
                        }

                        this.drag = {
                            down: evt.screenCoords, // Coordinates where drag started
                            dragged: false, // Actually dragged after threshold has been reached ?
                            to: this.#pickedRMCorner.point(), // Cell where drag ended
                            corner: this.#pickedRMCorner, // Cell where drag started
                            rmCell: { // the picked cell and its intial colum/row sizes
                                startRect: this.#rmCells[this.#pickedRMCorner.rmCell].range.rect(),
                                startSize: {
                                    cols: columns,
                                    rows: rows
                                }
                            },
                            context: { // areas around the cell; used to calculate their new size later on
                                left: prepareContext([rmCell.range.from.col - 1, rmCell.range.from.row], [0, rmCell.range.to.row]),
                                right: prepareContext([rmCell.range.to.col + 1, rmCell.range.from.row], [this.#columns.length - 1, rmCell.range.to.row]),
                                top: prepareContext([rmCell.range.from.col, 0], [rmCell.range.to.col, rmCell.range.from.row - 1]),
                                bottom: prepareContext([rmCell.range.from.col, rmCell.range.to.row + 1], [rmCell.range.to.col, this.#rows.length - 1])
                            }
                        };
                    } else {
                        let from;
                        if (this.#pickedRMCorner.corner === 0)
                            from = [rmCell.range.to.col, rmCell.range.to.row];
                        else if (this.#pickedRMCorner.corner === 1)
                            from = [rmCell.range.from.col, rmCell.range.to.row];
                        else if (this.#pickedRMCorner.corner === 2)
                            from = [rmCell.range.from.col, rmCell.range.from.row];
                        else if (this.#pickedRMCorner.corner === 3)
                            from = [rmCell.range.to.col, rmCell.range.from.row];
                        this.drag = {
                            down: evt.screenCoords, // Coordinates where drag started
                            dragged: false, // Actually dragged after threshold has been reached ?
                            from: from, // Cell where drag started
                            to: [this.#pickedCol, this.#pickedRow], // Cell where drag ended,
                            oldRMCell: rmCell,
                            startColRowWidths: {columns: columns, rows: rows}
                        };
                    }
                    return true;
                }
            } else if (evt.button === 1) {
                if (this.#pickedRMCell !== -1) return true; // Event consumed
            }
        }

        // Event not consumed
    }

    ontouchstart(evt) {
        evt.button = 0;
        return this.onmousedown(evt); // Emulate left-button down
    }

    onmouseup(evt) {
        let handled = false;

        if (this.drag && this.drag.dragged) { // Has the drag been actually performed
            if (!this.drag.corner) { // if it's a corner drag, we don't have to do anything on drag up
                // Create a new responsive cell
                const range = new MatrixRange(this, this.drag.from.slice(), this.drag.to.slice());

                if (range.checkRange()) {
                    if (this.drag.oldRMCell) {
                        const oldRMCell = this.drag.oldRMCell;
                        this.removeRMCell(this.#pickedRMCell)

                        range.foreachColumn(col => col._width.target[0] = this.drag.startColRowWidths.columns[0]);
                        range.foreachRow(row => row._height.target[0] = this.drag.startColRowWidths.rows[0]);

                        const cellType = oldRMCell instanceof UnitCell ? UnitCell : MetaCell;
                        const newRMCell = this.addRMCell(range, this.drag.down, cellType, 1);
                        newRMCell.activeTechnique = oldRMCell.activeTechnique;
                        newRMCell.activeLayout = oldRMCell.activeLayout;
                    } else {
                        if (evt.shiftKey) {
                            // Create multiple cells, one for each cell in the range
                            this.addRMCell(range, this.drag.down, UnitCell);
                        } else {
                            // Create one meta cell for the entire range
                            this.addRMCell(range, this.drag.down, MetaCell);
                        }
                    }
                }
            } else if (this.drag.context) {
                for (const side in this.drag.context) {
                    const range = this.drag.context[side].range;
                    if (range !== null) range.release();
                }
            }

            delete this.click; // Drag was performed, so this up event should NOT become a click
            handled = true;
        }

        if (this.click) {
            if (evt.button === 0) {
                // Create a new responsive cell
                let range = new MatrixRange(this, [this.#pickedCol, this.#pickedRow]);

                if (range.checkRange()) {
                    this.addRMCell(range, this.click.down, UnitCell);
                }
                handled = true;
            } else if (evt.button === 1) {
                // Middle click removes cells and releases locks of rows and columns
                // also closes local menu of cell
                if (this.#pickedRMCell !== -1) {
                    this.removeRMCell(this.#pickedRMCell);
                    handled = true;
                }
            }
        }

        delete this.drag;
        delete this.click;

        return handled; // Event consumed
    }

    ontouchend(evt) {
        evt.button = 0;
        return this.onmouseup(evt); // Emulate left-button up
    }

    onmousemove(evt) {
        if (this.drag) { // Could this be a drag

            // Only consider a drag when the cursor has been moved far enough
            const dx = this.drag.down[0] - evt.screenCoords[0];
            const dy = this.drag.down[1] - evt.screenCoords[1];
            this.drag.dragged = (this.drag.dragged || dx >= Constants.DRAG_THRESHOLD || dx < -Constants.DRAG_THRESHOLD || dy >= Constants.DRAG_THRESHOLD || dy < -Constants.DRAG_THRESHOLD);

            if (this.drag.dragged) {
                let newTo = this.cellAt(evt.screenCoords); // Set "to" to where the cursor has been dragged
                if (newTo[0] === -1) { // Limit "to" column to right-most column
                    if (evt.screenCoords[0] < this.#bounds.left) {
                        newTo[0] = 0;
                    } else if (evt.screenCoords[0] > this.#bounds.right) {
                        newTo[0] = this.#columns.length - 1;
                    }
                }
                if (newTo[1] === -1) { // Limit "to" row to bottom-most row
                    if (evt.screenCoords[1] < this.#bounds.top) {
                        newTo[1] = 0;
                    } else if (evt.screenCoords[1] > this.#bounds.bottom) {
                        newTo[1] = this.#rows.length - 1;
                    }
                }

                if (!this.drag.ignoreX) this.drag.to[0] = newTo[0]; // Set new "to" column (if not ignored)
                if (!this.drag.ignoreY) this.drag.to[1] = newTo[1]; // Set new "to" row (if not ignored)

                if (this.drag.corner) {
                    const curPos = evt.screenCoords;

                    // get the cell that is dragged
                    const rmCell = this.#rmCells[this.drag.corner.rmCell];
                    // check which corner is the traget
                    const curCorner = this.drag.corner.corner;

                    const dragToRight = curCorner === 1 || curCorner === 2;
                    const dragToBottom = curCorner === 2 || curCorner === 3;


                    const diffX = dragToRight ? curPos[0] - this.drag.rmCell.startRect.right : this.drag.rmCell.startRect.left - curPos[0];
                    const diffY = dragToBottom ? curPos[1] - this.drag.rmCell.startRect.bottom : this.drag.rmCell.startRect.top - curPos[1];


                    const contextX = dragToRight ? this.drag.context.right : this.drag.context.left;

                    if (contextX.range !== null) {
                        let contextXWidthFlexible = 0;
                        let contextXWidthFixed = 0;
                        let contextXWidth = 0;

                        let i = 0;
                        contextX.range.foreachColumn(col => {
                            if (col.locked <= 1) {
                                contextXWidthFlexible = contextXWidthFlexible + contextX.startSize.cols[i];
                            } else {
                                contextXWidthFixed = contextXWidthFixed + contextX.startSize.cols[i];
                            }
                            contextXWidth = contextXWidth + contextX.startSize.cols[i];
                            i++;
                        });

                        let factorContextX = (contextX.startRect.width - diffX) / contextX.startRect.width;
                        const targetContextXWidth = contextXWidth * factorContextX;

                        let scaleX = true;
                        if (targetContextXWidth <= 0) {
                            scaleX = false;
                        }

                        let scaleLockedX = false;
                        if (targetContextXWidth <= contextXWidthFixed) {
                            // scale all
                            scaleLockedX = true;
                        } else {
                            // scale flexible ones
                            let w = targetContextXWidth - contextXWidthFixed;
                            factorContextX = w / contextXWidthFlexible;
                        }

                        const factorX = (this.drag.rmCell.startRect.width + diffX) / this.drag.rmCell.startRect.width;

                        const minWidth = this.#_bounds.width / this.#columns.length;

                        scaleX = scaleX ? this.drag.rmCell.startSize.cols[0] * factorX > minWidth : scaleX;

                        if (scaleX) {
                            i = 0;
                            rmCell.range.foreachColumn(col => {
                                const newWidth = this.drag.rmCell.startSize.cols[i] * factorX;
                                col._width.animate([Math.max(minWidth, newWidth)]);
                                i++;
                            });

                            i = 0;
                            contextX.range.foreachColumn(col => {
                                if (scaleLockedX || col.locked <= 1) {
                                    const w = contextX.startSize.cols[i] * factorContextX;
                                    col._width.animate([w]);
                                } else {
                                    const w = contextX.startSize.cols[i];
                                    col._width.animate([w]);
                                }
                                i++;
                            });
                        }
                    }

                    const contextY = dragToBottom ? this.drag.context.bottom : this.drag.context.top;

                    if (contextY.range !== null) {
                        let contextYHeightFlexible = 0;
                        let contextYHeightFixed = 0;
                        let contextYHeight = 0;

                        let i = 0;
                        contextY.range.foreachRow(row => {
                            if (row.locked <= 1) {
                                contextYHeightFlexible = contextYHeightFlexible + contextY.startSize.rows[i];
                            } else {
                                contextYHeightFixed = contextYHeightFixed + contextY.startSize.rows[i];
                            }
                            contextYHeight = contextYHeight + contextY.startSize.rows[i];
                            i++;
                        });

                        let factorContextY = (contextY.startRect.height - diffY) / contextY.startRect.height;
                        const targetContextYHeight = contextYHeight * factorContextY;

                        let scaleY = true;
                        if (targetContextYHeight <= 0) {
                            scaleY = false;
                        }

                        let scaleLockedY = false;
                        if (targetContextYHeight <= contextYHeightFixed) {
                            // scale all
                            scaleLockedY = true;
                        } else {
                            // scale flexible ones
                            const h = targetContextYHeight - contextYHeightFixed;
                            factorContextY = h / contextYHeightFlexible;
                        }


                        const factorY = (this.drag.rmCell.startRect.height + diffY) / this.drag.rmCell.startRect.height;

                        const minHeight = this.#_bounds.height / this.#rows.length;

                        scaleY = scaleY ? this.drag.rmCell.startSize.rows[0] * factorY > minHeight : scaleY;

                        if (scaleY) {
                            i = 0;
                            rmCell.range.foreachRow(row => {
                                const newHeight = this.drag.rmCell.startSize.rows[i] * factorY;
                                row._height.animate([Math.max(minHeight, newHeight)]);
                                i++;
                            });

                            i = 0;
                            contextY.range.foreachRow(row => {
                                if (scaleLockedY || row.locked <= 1) {
                                    const h = contextY.startSize.rows[i] * factorContextY;
                                    row._height.animate([h]);
                                } else {
                                    const h = contextY.startSize.rows[i];
                                    row._height.animate([h]);
                                }
                                i++;
                            });
                        }
                    }


                    this.#layout.invalidate();
                }
            }
            return true; // Event consumed
        }

        // Event not consumed
    }

    ontouchmove(evt) {
        return this.onmousemove(evt); // Emulate mouse move
    }

    onwheel(evt) {
        if (evt.shiftKey || evt.ctrlKey) {
            if (this.#pickedRMCell !== -1) {
                // Scale all cells under the cursor
                const rmCell = this.#rmCells[this.#pickedRMCell];
                const delta = evt.deltaY ? evt.deltaY : evt.deltaX;
                const factor = delta > 0 ? Constants.ZOOM_WHEEL_FACTOR : 1 / Constants.ZOOM_WHEEL_FACTOR;
                const minWidth = this.#_bounds.width / this.#columns.length;
                const minHeight = this.#_bounds.height / this.#rows.length;
                if (evt.shiftKey) rmCell.range.foreachColumn(col => col._width.animate([Math.max(minWidth, col._width.target[0] * factor)]));
                if (evt.ctrlKey) rmCell.range.foreachRow(row => row._height.animate([Math.max(minHeight, row._height.target[0] * factor)]));

                this.#layout.invalidate();
                return true; // Event consumed
            }
        }

        // Event not consumed
    }

    onclick(evt) {
        return true; // Event consumed
    }

    ondblclick(evt) {
        return true; // Event consumed
    }

    onkeydown(evt) {
        if (this.#pickedRMCell !== -1) {
            switch (evt.key) {
                case "ArrowLeft":
                    this.#rmCells[this.#pickedRMCell].activeTechnique--;
                    this.menu.update();
                    return true;
                case "ArrowRight":
                    this.#rmCells[this.#pickedRMCell].activeTechnique++;
                    this.menu.update();
                    return true;
                case "ArrowUp":
                    if (this.#rmCells[this.#pickedRMCell].activeLayout !== undefined) this.#rmCells[this.#pickedRMCell].activeLayout--;
                    this.menu.update();
                    return true;
                case "ArrowDown":
                    if (this.#rmCells[this.#pickedRMCell].activeLayout !== undefined) this.#rmCells[this.#pickedRMCell].activeLayout++;
                    this.menu.update();
                    return true;
                case " ":
                    this.toggleRMCellType();
                    return true;
                case "Tab":
                    this.toggleRMCellEntityType();
                    return true;
                case "Delete":
                    this.removeRMCell(this.#pickedRMCell);
                    return true;
                case 'r':
                    let len = this.#rmCells.length;
                    this.#pickedRMCell = -1;
                    for (let i = 0; i < len; i++) {
                        this.removeRMCell(0);
                    }
                    return true;
            }
        } else {
            switch (evt.key) {
                case 'r':
                    let len = this.#rmCells.length;
                    for (let i = 0; i < len; i++) {
                        this.removeRMCell(0);
                    }
                    return true;
            }
        }
        return false; // Event not consumed
    }

    oncontextment(evt) {
        if (this.#pickedRMCell !== -1 && !this.#viewport.lastRightMouseWasDrag)
            this.menu.show([evt.clientX, evt.clientY], this, this.#pickedRMCell);
        return false; // Return false to prevent standard context menu
    }

    onclose() {
        this.#adjacency.forEach(dot => dot.node.highlight.delete(this));
        this.#graph.edges.forEach(edge => edge.highlight.delete(this));
    }

    update(time) {
        let needUpdate;

        needUpdate = this.#layout.step(time);

        this.#columns.forEach(col => {
            needUpdate = col._x.update(time) || needUpdate;
            needUpdate = col._width.update(time) || needUpdate;
        });

        this.#rows.forEach(row => {
            needUpdate = row._y.update(time) || needUpdate;
            needUpdate = row._height.update(time) || needUpdate;
        });

        this.project(); // Project must take place before cells update themselves, because they depend on the new screen coordinates

        this.#rmCells.forEach(rmc => {
            needUpdate = rmc.update(time) || needUpdate;
        });

        return needUpdate;
    }

    project() {

        this.#bounds.set(...this.#viewport.project(this.#_bounds));

        this.#columns.forEach(col => {
            col.x = this.#viewport.projectX(col._x.value[0]);
            col.width = this.#viewport.projectWidth(col._width.value[0]);
        });

        this.#rows.forEach(row => {
            row.y = this.#viewport.projectY(row._y.value[0]);
            row.height = this.#viewport.projectHeight(row._height.value[0]);
        });

    }

    cellColor(i, j) {
        const col = this.#columnOrder[i];
        const row = this.#rowOrder[j];
        // gc.fillStyle = this.#adjacency[col][row].color; // Full matrix shows adjacency
        // gc.fillStyle = (this.#similarity) ? this.#similarity[col][row].color : 'cyan'; // Full matrix shows similarity
        return (i <= j) ? this.#adjacency[col][row].color : (this.#similarity) ? this.#similarity[col][row].color : 'cyan'; // Half adjacency half similarity
    }

    drawCells(gc) {
        let n = this.#adjacency.length;

        gc.clearRect(this.#bounds.x, this.#bounds.y, this.#bounds.width, this.#bounds.height);

        for (let i = 0; i < n; i++) {
            for (let j = 0; j < n; j++) {
                let col = this.#columnOrder[i];
                let row = this.#rowOrder[j];

                gc.fillStyle = this.cellColor(i, j);
                let b = this.cellBoundsNoTearing(i, j);
                gc.fillRect(b[0], b[1], Math.max(1, b[2]), Math.max(1, b[3]));
            }
        }

        let w = this.#bounds.width / this.#columns.length;
        let t = Math.max(0, Math.min((w - 5) / 30, 1));
        gc.strokeStyle = `rgba(160, 160, 160, ${t})`;

        gc.beginPath();
        for (let i = 0; i < n; i++) {
            let x = this.#columns[this.#columnOrder[i]].x;
            gc.moveTo(x, this.#bounds.top);
            gc.lineTo(x, this.#bounds.bottom);
            let y = this.#rows[this.#rowOrder[i]].y;
            gc.moveTo(this.#bounds.left, y);
            gc.lineTo(this.#bounds.right, y);
        }
        gc.moveTo(this.#bounds.right, this.#bounds.top);
        gc.lineTo(this.#bounds.right, this.#bounds.bottom);
        gc.moveTo(this.#bounds.left, this.#bounds.bottom);
        gc.lineTo(this.#bounds.right, this.#bounds.bottom);
        gc.stroke();

        const strokeRange = (range) => {
            gc.strokeRect(...range.rect());
        };

        gc.save();
        gc.shadowColor = '#101010';
        gc.shadowOffsetX = 1;
        gc.shadowOffsetY = 1;
        gc.shadowBlur = 2;

        if (this.drag && this.drag.dragged && (!this.drag.corner || this.drag.oldRMCell)) {
            gc.strokeStyle = '#303030';
            let range = new MatrixRange(this, this.drag.from.slice(), this.drag.to.slice());
            if (range.checkRange()) {
                strokeRange(range.shadowRange());
                strokeRange(range);
            }
        } else {
            gc.strokeStyle = '#F0F0F0';
            if (this.#pickedCol !== -1 && this.#pickedRMCell === -1) {
                gc.strokeRect(this.#columns[this.#columnOrder[this.#pickedCol]].x, this.#bounds.top, this.#columns[this.#columnOrder[this.#pickedCol]].width, this.#bounds.height);
            }

            if (this.#pickedRow !== -1 && this.#pickedRMCell === -1) {
                gc.strokeRect(this.#bounds.left, this.#rows[this.#rowOrder[this.#pickedRow]].y, this.#bounds.width, this.#rows[this.#rowOrder[this.#pickedRow]].height);
            }
        }
        gc.restore();


        gc.save();
        if (this.#pickedRMCell !== -1) {
            gc.shadowColor = '#404040';
            gc.shadowOffsetX = 2;
            gc.shadowOffsetY = 2;
            gc.shadowBlur = 4;

            gc.strokeStyle = '#606060';
            strokeRange(this.#rmCells[this.#pickedRMCell].range.shadowRange()); // Shadow ranges must be drawn first to avoid overdrawing highlighted cell
        }
        gc.restore();

        this.#rmCells.forEach(rmc => {
            // skip drawing of cell if it's currently getting expanded/shrinked
            if (this.drag && this.drag.oldRMCell && rmc === this.drag.oldRMCell)
                return;
            gc.save();
            rmc.render(gc);
            gc.restore();

            gc.save();
            gc.lineWidth = 1.5;
            gc.strokeStyle = '#808080';
            strokeRange(rmc.range);
            gc.restore();
        });

        if (this.#pickedRMCell !== -1) {
            gc.save();
            gc.shadowColor = '#404040';
            gc.shadowOffsetX = 2;
            gc.shadowOffsetY = 2;
            gc.shadowBlur = 4;
            gc.strokeStyle = '#303030';
            strokeRange(this.#rmCells[this.#pickedRMCell].range);

            gc.restore();
        }
    }

    drawLabels(gc) {
        let n = this.#adjacency.length;

        if (this.#adjacency[0] === undefined) return;

        let node;
        let size;

        let setFont = (node, rowcol, size) => {
            let t = Math.max(0, Math.min(size / 5, 1));
            let gray = (rowcol.locked) ? 90 : 130;
            gc.fillStyle = (node.highlight.size > 0) ? 'red' : `rgba(${gray}, ${gray}, ${gray}, ${t})`;
            gc.font = `${size}px sans-serif`;
        };

        for (let i = 0; i < n; i++) {
            gc.save();
            let col = this.#columns[this.#columnOrder[i]];
            node = this.#adjacency[this.#columnOrder[i]].node;
            size = Math.min(24, col.width * ((node.highlight.size > 0) ? 1.2 : .8));
            setFont(node, col, size);

            gc.translate(col.x + .5 * col.width, this.#bounds.top - size * .25);
            gc.rotate(-90 * Math.PI / 180);
            gc.textBaseline = 'middle';
            gc.fillText(node.label, 0, 0);
            gc.restore();

            gc.save();
            let row = this.#rows[this.#rowOrder[i]];
            node = this.#adjacency[this.#rowOrder[i]].node;
            size = Math.min(24, row.height * ((node.highlight.size > 0) ? 1.2 : .8));
            setFont(node, row, size);

            gc.translate(this.#bounds.left - gc.measureText(node.label).width - size * .25, row.y + .5 * row.height);
            gc.textBaseline = 'middle';
            gc.fillText(node.label, 0, 0);
            gc.restore();
        }
    }

    drawToolTip(gc) {
        if (this.#pickedCol === -1 || this.#pickedRow === -1) return;

        gc.strokeStyle = 'white';
        gc.fillStyle = 'gray';
        let adj = this.#adjacency[this.#columnOrder[this.#pickedCol]][this.#rowOrder[this.#pickedRow]];
        let sim = (this.#similarity) ? this.#similarity[this.#columnOrder[this.#pickedCol]][this.#rowOrder[this.#pickedRow]] : {value: 'Computing...'};
        adj = "Edge: " + ((adj.edge !== null) ? adj.edge.source.label + " --> " + adj.edge.target.label : "×");
        sim = "Similarity: " + sim.value;
        let tooltip = adj + " / " + sim;
        gc.fillStyle = `rgba(${.7}, ${.7}, ${.7}, ${1})`;
        gc.font = `${14}px sans-serif`;
        let y = this.#viewport.window.y + this.#viewport.window.height - 20;
        let x = this.#viewport.window.x + this.#viewport.window.width / 2 - gc.measureText(tooltip).width / 2;
        gc.fillText(tooltip, x, y);
    }

    drawCorners(gc) {
        if (this.#pickedRMCorner != null) {
            gc.save();
            gc.strokeStyle = '#303030';
            gc.fillStyle = 'white';
            gc.lineWidth = 1;
            gc.shadowColor = '#404040';
            gc.shadowOffsetX = 2;
            gc.shadowOffsetY = 2;
            gc.shadowBlur = 4;
            gc.beginPath();
            gc.arc(...this.#pickedRMCorner.point(), Constants.PICK_TOLERANCE, 0, 2 * Math.PI);
            gc.fill();
            gc.stroke();
            gc.restore();
        }

    }

    draw(gc) {
        this.drawCells(gc);
        this.drawLabels(gc);
        //this.drawToolTip(gc);
        this.drawCorners(gc);
    }

    shuffle() {

        // Taken from https://bost.ocks.org/mike/shuffle/
        function shuffle(array) {
            let m = array.length, t, i;

            // While there remain elements to shuffle…
            while (m) {

                // Pick a remaining element…
                i = Math.floor(Math.random() * m--);

                // And swap it with the current element.
                t = array[m];
                array[m] = array[i];
                array[i] = t;
            }

            return array;
        }

        // Clear all cells
        this.#pickedRMCell = -1;
        while (this.#rmCells.length > 0) {
            this.removeRMCell(0);
        }

        shuffle(this.#columnOrder);
        this.#rowOrder = this.#columnOrder;
        // shuffle(this.#rowOrder);

        this.#layout.invalidate();
    }

    order(sortby, ascending) {

        // Clear all cells
        this.#pickedRMCell = -1;
        while (this.#rmCells.length > 0) {
            this.removeRMCell(0);
        }

        let m = this.#columnOrder.length;
        let tobesorted = [];

        switch (sortby) {
            case "name":
                for (let i = 0; i < m; i++) {
                    tobesorted[i] = this.#adjacency[i].node;
                }

                tobesorted.sort(function (a, b) {
                    let name_a = a['label'].split(' ').slice(-1).join(' '); // Extract last names from labels
                    let name_b = b['label'].split(' ').slice(-1).join(' ');
                    if (name_a < name_b) return -1;
                    else if (name_a > name_b) return 1;
                    return 0;
                });
                break;
            case "similarity":
                for (let i = 0; i < m; i++) {
                    tobesorted[i] = {sum: 0};
                    tobesorted[i].id = i;
                    for (let j = 0; j < m; j++) {
                        tobesorted[i].sum += this.#similarity[i][j].value;
                    }
                }

                tobesorted.sort(function (a, b) {
                    return a.sum - b.sum;
                });
                break;
            case "rcm":
                tobesorted = this.reverse_cuthill_mckee();
                break;
            default:
                for (let i = 0; i < m; i++) {
                    tobesorted[i] = this.#adjacency[i].node;
                }

                tobesorted.sort(function (a, b) {
                    return a[sortby] - b[sortby];
                });
                break;
        }

        for (let i = 0; i < m; i++) {
            this.#columnOrder[i] = tobesorted[i].id;
        }

        if (!ascending) this.#columnOrder.reverse();
        this.#rowOrder = this.#columnOrder;

        this.#layout.invalidate();
    }

    reverse_cuthill_mckee() {

        let temp = [];
        let visited = [];
        let start;
        let order = [];

        //breadth first search
        function bfs(v, visited, adjacency) {
            let q = [];
            let nodes = [];

            //get first node
            nodes.push(v);

            //order neighbors of v by degree and add to q
            let neigh = getSortedNeighbors(v, adjacency);
            q = q.concat(neigh);
            visited[v.id] = true;

            while (q.length > 0) {
                v = q.shift();
                if (visited[v.id]) continue;
                neigh = getSortedNeighbors(v, adjacency);
                q = q.concat(neigh);

                nodes.push(v);
                visited[v.id] = true;
            }
            return nodes;
        }

        //get neighbors in ascending order
        function getSortedNeighbors(node, adjacency) {
            let arr = [];
            for (let i = 0; i < node.neighbors.length; i++) {
                let n = node.neighbors[i];
                arr[i] = adjacency[n].node;
            }

            arr.sort(function (a, b) {
                return a.degree - b.degree;
            });

            return arr;
        }

        for (let i = 0; i < this.#columnOrder.length; i++) {
            temp[i] = this.#adjacency[i].node;
        }

        temp.sort(function (a, b) {
            return a.degree - b.degree;
        });

        //traverse nodes
        for (let i = 0; i < temp.length; i++) {
            start = temp[i];
            if (!visited[start.id]) order = [...order, ...bfs(start, visited, this.#adjacency).reverse()];
        }

        return order;
    }
}
