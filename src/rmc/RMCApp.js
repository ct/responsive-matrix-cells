import {AppBase} from "../base/AppBase.js";
import {Graph, SoccerGraph} from "../base/Graph.js";
import {Matrix} from "./Matrix.js";
import {Menu} from "./Menu.js";
import {Constants} from "../base/Constants.js";
import {ColorSelector} from "../base/ColorSelector.js";

export class RMCApp extends AppBase {

    constructor(container, options = {}) {
        super(container, options);
        this.load(new SoccerGraph());

        this.canvas.oncontextmenu = (evt) => this.matrix.oncontextment(evt);
    }

    get interactables() {
        return [this.matrix];
    }

    data(graph) {
        this.matrix = new Matrix(graph, this.viewport, this.refresh());
        //this.matrix = new BarChart2(graph, this.viewport, this.refresh(),() => '#FFFFFFFF');
        this.reset();
    }

    init(container, options) {
        this.data(Graph.emptyGraph());

        // disable grid
        this.grid.active = false;
    }

    ui(container, options) {

        document.getElementById('help').addEventListener("click", () => {
            document.getElementById('controls').scrollIntoView({'behavior': 'smooth'});
            document.getElementById('help').remove();
        });

        // Populate attribute selection dropdowns with options based on data attributes
        Object.entries(SoccerGraph.LABELS[Graph.NODE]).forEach(entry => {
            let [attr, label] = entry;
            $('#similarity-attributes-selection').append(`<option value="${attr}">${label}</option>`);
            if (attr !== 'degree') $('#sort-attribute-selection').append(`<option value="${attr}">${label}</option>`);
        });

        const sort = () => {
            let sortby = document.getElementById('matrix-order-accordion').querySelector('.content.active').attributes['data-sort'].value;
            if (sortby === 'structure') {
                sortby = document.getElementById('sort-structure-selection').value; // Check which structure aspect is selected for sorting
            } else if (sortby === 'attribute') {
                sortby = document.getElementById('sort-attribute-selection').value; // Check which attribute is selected for sorting
            }

            let ascending = document.getElementById('sort-order-toggle').querySelector('input').checked;

            this.matrix.order(sortby, ascending);
            this.requestUpdate();
        }

        // Set up accordion for matrix sorting
        $('#matrix-order-accordion').accordion({
            collapsible: false, // Prevent active accordion entry to be collapsed so that current sorting order is always clear to user
            onOpen: sort, // onOpen is called only after the accordion animation has finished, which causes a minimal delay
        });

        // Set up dropdown for structure-based sorting
        $('#sort-structure-selection').dropdown({
            onChange: sort
        });

        // Set up dropdown for attribute-based sorting
        $('#sort-attribute-selection').dropdown({
            onChange: sort
        });
        $('#sort-attribute-selection').dropdown('set selected', 'appearance'); // Select initial option

        // Set up checkbox for changing sorting order
        $('#sort-order-toggle').checkbox({
            onChange: sort
        });

        // Set up dropdown for attributes to be considered for similarity computation
        $('#similarity-attributes-selection').dropdown({
            onChange: (value, text, $choice) => {
                let selAttrs = $('#similarity-attributes-selection').val();

                let sortby = document.getElementById('matrix-order-accordion').querySelector('.content.active').attributes['data-sort'].value;

                this.matrix.encodeSimilarity(selAttrs, sortby === 'similarity' ? sort : undefined); // Automatically re-sort when the similarity attributes have changed and the sorting order is based on similarity

                // Set default attribute selection for local menu
                Menu.attrs[Graph.NODE] = [...selAttrs];

                // Link attribute selection in side menu to local menus and rmc
                if ($('#link-attributes-checkbox').checkbox('is checked')) {
                    $('#float-select').dropdown('set exactly', selAttrs);
                    this.matrix.changeSimRMCellAttributes(selAttrs);
                }

                this.requestUpdate();
            }
        });
        $('#similarity-attributes-selection').dropdown('set selected', Constants.DEFAULT_NODE_ATTRIBUTES);

        // Set up checkbox for automatic attribute-selection linking
        $('#link-attributes-checkbox').checkbox({
            onChecked: () => {
                let selAttrs = $('#similarity-attributes-selection').val();
                $('#float-select').dropdown('set exactly', selAttrs);
                this.matrix.changeSimRMCellAttributes(selAttrs);
                this.requestUpdate();
            }
        });

        // Set up color map selectors
        let similarity_cs = new ColorSelector($("#similarity-color-selector"), "RdYlGn", "Node-Similarity");
        let edge_cs = new ColorSelector($("#edge-color-selector"), "Blues", "Edge-Attributes");

        // Change similarity color scale on selection
        let entries = similarity_cs.list.children;
        for (let i = 0; i < entries.length; i++) {
            $(entries[i]).click((evt) => {
                this.matrix.similarityColorScale = evt.target.textContent;

                this.matrix.updateSimilarityColorEncoding();
                this.requestUpdate();
            });
        }

        // Change edge attribute color scale on selection
        entries = edge_cs.list.children;
        for (let i = 0; i < entries.length; i++) {
            $(entries[i]).click((evt) => {
                this.matrix.edgeColorScale = evt.target.textContent;

                this.matrix.changeEdgeWeightColorScale();
                this.requestUpdate();
            });
        }
    }

    reset() {
        this.viewport.fitToView(this.matrix.bounds(), true, true);
        this.requestUpdate();
    }

    update(time) {
        let needUpdate = this.matrix.update(time);

        this.gc.clearRect(this.window.x, this.window.y, this.window.width, this.window.height);
        this.grid.draw(this.gc);
        this.matrix.draw(this.gc);

        // Request next animation frame if needed
        if (needUpdate) this.requestUpdate();
    }

    resize(width, height) {

    }

    onkeypress(evt) {
        if (super.onkeypress(evt)) return true;

        switch (evt.key) {
            case ' ':
                //this.matrix.shuffle();
                return true;
        }
        return false; // Event not consumed
    }
}