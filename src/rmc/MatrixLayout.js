export function MatrixLayout(matrix) {

    let done = false;

    // TODO: Think about a more clever way of distributing space to free rows and columns

    this.propagate = function () {
        let [matrixX, matrixY, matrixWidth, matrixHeight] = matrix.bounds();
        let usedWidth = 0;
        let usedHeight = 0;
        let freeRows = 0;
        let freeCols = 0;

        const n = matrix.adjacency.length;
        for (let i = 0; i < n; i++) {
            let col = matrix.column(i);
            let row = matrix.row(i);
            if (col.locked > 0) {
                usedWidth += col._width.target[0];
            } else {
                freeCols++;
            }
            if (row.locked > 0) {
                usedHeight += row._height.target[0];
            } else {
                freeRows++;
            }
        }

        let availableWidth = matrixWidth - usedWidth;
        let availableHeight = matrixHeight - usedHeight;

        // Reduce fixed cells if their accumulated width exceeds matrix width
        if (availableWidth < 0) {
            let reduceBy = 1 / (usedWidth / matrixWidth);
            usedWidth = 0;
            for (let i = 0; i < n; i++) {
                let col = matrix.column(i);
                if (col.locked > 0) {
                    let w = col._width.target[0] * reduceBy;
                    col._width.animate([w]);
                    usedWidth += w;
                }
            }
            availableWidth = matrixWidth - usedWidth;
        }

        if (availableHeight < 0) {
            let reduceBy = 1 / (usedHeight / matrixHeight);
            usedHeight = 0;
            for (let j = 0; j < n; j++) {
                let row = matrix.row(j);
                if (row.locked > 0) {
                    let h = row._height.target[0] * reduceBy;
                    row._height.animate([h]);
                    usedHeight += h;
                }
            }
            availableHeight = matrixHeight - usedHeight;
        }

        let newCellWidth = availableWidth / freeCols;
        let newCellHeight = availableHeight / freeRows;
        let x = matrixX;
        let y = matrixY;
        for (let i = 0; i < n; i++) {
            let col = matrix.column(i);
            let row = matrix.row(i);

            col._x.animate([x]);
            row._y.animate([y]);
            if (col.locked === 0) {
                col._width.animate([newCellWidth]);
            }
            if (row.locked === 0) {
                row._height.animate([newCellHeight]);
            }
            x += col._width.target[0];
            y += row._height.target[0];
        }
    };

    this.invalidate = function () {
        done = false;
    };

    this.step = function (time) {
        if (done) return false;
        this.propagate();
        done = true;
        return !done;
    };
}