export class BarLayout {

    static HORIZONTAL = 0;
    static VERTICAL = 1;
    static ARRANGEMENTS = ['horizontal', 'vertical'];
    static BAR_VECTOR = [[0, 1], [1, 0]];  // Vector in the direction of the bar [0,1] (horizontal) or [1,0] (vertical)

    #done;
    #arrangement;
    #barchart;

    constructor(barchart, arrangement) {
        this.#done = false;
        this.#arrangement = arrangement;
        this.#barchart = barchart;
    }

    groupedbars(dim = 'width') {
        const horizontal = (dim === 'width');
        const b = this.#barchart.bounds();
        const n = this.#barchart.data.attributes.length;

        if (n === 1) {
            this.#barchart.groups[0]._bounds.set(...b);
            if (horizontal) {
                this.#barchart.axes[0]._start.animate([b.left, b.bottom]);
                this.#barchart.axes[0]._end.animate([b.left, b.top]);
            } else {
                this.#barchart.axes[0]._start.animate([b.left, b.top]);
                this.#barchart.axes[0]._end.animate([b.right, b.top]);
            }

        } else {
            const gap = b[dim] / (this.#barchart.data.attributes.length);
            this.#barchart.groups.forEach((grp, i) => {
                if (horizontal) {
                    grp._bounds.set(b.left + i * gap, b.top, gap, b.height);
                    this.#barchart.axes[i]._start.animate([grp._bounds.left, grp._bounds.bottom]);
                    this.#barchart.axes[i]._end.animate([grp._bounds.left, grp._bounds.top]);
                } else {
                    grp._bounds.set(b.left, b.top + i * gap, b.width, gap);
                    this.#barchart.axes[i]._start.animate([grp._bounds.left, grp._bounds.top]);
                    this.#barchart.axes[i]._end.animate([grp._bounds.right, grp._bounds.top]);
                }
            });
        }

        this.#barchart.groups.forEach((grp, i) => {
            const gap = grp._bounds[dim] / (this.#barchart.data.tuples.length + 1);
            this.#barchart.data.tuples.forEach((tuple, j) => {
                const bar = this.#barchart.bars[i][j];
                if (horizontal) {
                    bar._anchor.animate([grp._bounds.left + (j + 1) * gap, grp._bounds.bottom]);
                } else {
                    bar._anchor.animate([grp._bounds.left, grp._bounds.top + (j + 1) * gap]);
                }
            });
        });

        if (horizontal) {
            this.#barchart.angle.animate([-Math.PI / 2]);
        } else {
            this.#barchart.angle.animate([0]);
        }
    }

    horizontal() {
        this.groupedbars('width');
    }

    vertical() {
        this.groupedbars('height');
    }

    vector() {
        return BarLayout.BAR_VECTOR[this.#arrangement];
    }

    update(time) {
        if (this.done) return false;
        this[BarLayout.ARRANGEMENTS[this.#arrangement]]();
        this.done = true;
        return !this.done;
    };

    invalidate() {
        this.done = false;
    };
}