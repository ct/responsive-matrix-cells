import {Aggregator} from "./Aggregator.js";
import {DataTable} from "./DataTable.js";

export class Graph {

    static NODE = 0;
    static EDGE = 1;

    #nodes = [];
    #edges = [];

    #labels = {
        [Graph.EDGE]: {
            weight: 'Weight',
        },
        [Graph.NODE]: {
            degree: 'Degree',
        }
    };

    #listeners = new Set();

    constructor() {
        this.#nodes.max = {};
        this.#nodes.min = {};

        this.#edges.max = {};
        this.#edges.min = {};
    }

    get nodes() {
        return this.#nodes;
    }

    get edges() {
        return this.#edges;
    }

    get labels() {
        return this.#labels;
    }

    static emptyGraph() {
        const g = new Graph();
        g.load = async () => g;
        g.prepare([], []);
        return g;
    }

    register(l) {
        this.#listeners.add(l);
    }

    unregister(l) {
        this.#listeners.delete(l);
    }

    onchange(type, target, attr, old, value) {
        this.#listeners.forEach(l => l(type, target, attr, old, value));
    }

    prepare(_nodes, _edges) {

        // Proxy handler to intercept property changes of nodes and edges
        let handler = (graph, type) => ({
            set(target, p, value, receiver) {
                // console.log(`Changed node value ${p} to ${value}.`);
                let old = target[p];
                target[p] = value;
                graph.onchange(type, target, p, old, value);
                return true;
            }
        });

        this.#nodes.length = 0;
        for (let i = 0; i < _nodes.length; i++) {
            // Install proxy to trap value changes
            this.#nodes[i] = new Proxy(_nodes[i], handler(this, Graph.NODE));
        }

        this.#edges.length = 0;
        for (let i = 0; i < _edges.length; i++) {
            // Install proxy to trap value changes
            this.#edges[i] = new Proxy(_edges[i], handler(this, Graph.EDGE));
        }

        this.#nodes.forEach((node, i) => {
            node.index = i;
            node.edges = [];
            node.neighbors = [];
            node.degree = 0;
            node.label = (node.label !== undefined) ? node.label : "Node " + i;
            node.highlight = new Set(); // Global flag for node highlighting
        });

        this.#edges.forEach((edge, i) => {
            let weight = edge.val !== undefined ? edge.val : 1;

            let n = this.#nodes[edge.src]; // The source node
            n.edges.push(i);  // Store index of incident edge at source node
            n.neighbors.push(edge.dst);  // Store index of source node as neighbor of destination node
            n.degree++; // Increment degree of source node

            n = this.#nodes[edge.dst]; // The destination node
            n.edges.push(i); // Store index of incident edge at destination node
            n.neighbors.push(edge.src); // Store index of destination node as neighbor of source node
            n.degree++; // Increment degree of destination node

            edge.index = i;
            edge.source = this.#nodes[edge.src];
            edge.target = this.#nodes[edge.dst];
            edge.weight = weight;
            edge.highlight = new Set(); // Global flag for edge highlighting
        });

        // this.#nodes.forEach(node => Object.freeze(node));
        // this.#edges.forEach(edge => Object.freeze(edge));

        const attrs = (entities) => [...entities.reduce((set, entity) => { // Collect attrs in a set and return elements as array
            Object.keys(entity).forEach((attr) => set.add(attr));
            return set;
        }, new Set())];

        const nodeattrs = attrs(this.#nodes);
        this.#nodes.forEach(node => nodeattrs.forEach(attr => node[attr] = (node[attr] !== undefined) ? node[attr] : -0)); // Set NULL values, -0 can be tested with Object.is(o, -0)

        const edgeattrs = attrs(this.#edges);
        this.#edges.forEach(edge => edgeattrs.forEach(attr => edge[attr] = (edge[attr] !== undefined) ? edge[attr] : -0)); // Set NULL values, -0 can be tested with Object.is(o, -0)

        this.aggregator = new Aggregator(this, nodeattrs, edgeattrs);
        this.min = this.aggregator.min;
        this.max = this.aggregator.max;
    }

    subgraph(entitytype, sub) { // sub either holds nodes or edges for deriving a subgraph
        let graph = new Graph();

        let subnodes, subedges;

        if (entitytype === Graph.NODE) { // Set of sub nodes is given, find the corresponding edges
            subnodes = sub;

            let nodeIndexMap = []; // Mapping of original node indices to new indices in subgraph
            subnodes.forEach((n, i) => {
                nodeIndexMap[n.index] = i;
            });

            subedges = [];
            this.edges.forEach(e => {
                if (nodeIndexMap[e.src] !== undefined && nodeIndexMap[e.dst] !== undefined) { // Find all edges incident to subnodes
                    // Create proxies for edges with new node index mapping
                    e = new Proxy(e, {  // Clone edge
                        get(target, p, receiver) {
                            if (p === 'src') return nodeIndexMap[target.src]; // Resolve node index mapping
                            if (p === 'dst') return nodeIndexMap[target.dst];
                            return target[p];
                        }
                    });
                    subedges.push(e);
                }
            });
        } else if (entitytype === Graph.EDGE) { // Set of sub edges is given, collect the corresponding nodes
            let n = new Set();
            sub.forEach(edge => {
                n.add(edge.source);
                n.add(edge.target);
            });
            subnodes = [...n];

            let nodeIndexMap = []; // Mapping of original node indices to new indices in subgraph
            subnodes.forEach((n, i) => {
                nodeIndexMap[n.index] = i;
            });

            subedges = [];
            sub.forEach(e => {
                // Create proxies for edges with new node index mapping
                e = new Proxy(e, {  // Clone edge
                    get(target, p, receiver) {
                        if (p === 'src') return nodeIndexMap[target.src]; // Resolve node index mapping
                        if (p === 'dst') return nodeIndexMap[target.dst];
                        return target[p];
                    }
                });
                subedges.push(e);
            });
        } else {
            throw "Either sub.nodes or sub.edges must be provided to subgraph!"
        }

        // Set nodes and edges of subgraph
        graph.#nodes = subnodes;
        graph.#edges = subedges;

        // Delegate all subgraph functionality to original graph (to avoid subgraphs of subgraphs)
        graph.#labels = this.#labels;
        graph.aggregator = this.aggregator;
        graph.min = this.min;
        graph.max = this.max;
        graph.subgraph = this.subgraph;

        return graph;
    }

    entities(type = Graph.NODE) {
        return (type === Graph.NODE) ? this.nodes : this.edges;
    }

    data(attr, type = Graph.NODE) {
        let labels = attr.map(a => this.labels[type][a]);
        return (type === Graph.NODE) ? new DataTable(this.nodes, attr, labels, this.aggregator.nodes) : new DataTable(this.edges, attr, labels, this.aggregator.edges);
    }

    normalize(o, attrs) {
        return attrs.reduce((n, attr) => {
            let min = this.min.node[attr];
            let max = this.max.node[attr];
            let range = max - min;
            n[attr] = (range !== 0) ? (o[attr] - min) / range : 0;
            return n;
        }, {});
    }
}

export class JSONGraph extends Graph {
    constructor(url) {
        super();
        this.url = url;
    }

    load() {
        return fetch(this.url)
            .then(response => {
                if (response.ok) {
                    return response.json();
                }
                throw new Error("Could not load data from " + this.url);
            })
            .then(json => {
                this.prepare(json.nodes, json.edges);
                return this;
            });
    }
}

export class SoccerGraph extends JSONGraph {

    static LABELS = {
        [Graph.EDGE]: {
            weight: 'Weight',
        },
        [Graph.NODE]: {
            degree: 'Degree',
            appearance: 'Appearances',
            mins_played: 'Minutes played',
            final_third: 'Final third',
            shots_total: 'Total shots',
            shot_on_target: 'On-target shots',
            shot_off_target: 'Off-target shots',
            goals: 'Goals',
            goal_normal: 'Goals (normal)',
            goal_head: 'Goals (head)',
            big_chance_scored: 'Big chance scored',
            penalty_scored: 'Penalties scored',
            assist: 'Assists',
            pass_key: 'Key passes',
            pass_accurate: 'Accurate passes',
            pass_inaccurate: 'Inaccurate passes',
            possession: 'Ball possession',
            touches: 'Touches',
            keeper_missed: 'Keeper missed',
            keeper_save_total: 'Keeper save total',
            clearance_total: 'Total clearances',
            challenge_lost: 'Challenges lost',
            interception_all: 'Interceptions',
            dispossessed: 'Dispossessed',
            ball_recovery: 'Balls recovered',
            dribble_won: 'Dribbles won',
            dribble_lost: 'Dribbles lost',
            tackle_won: 'Tackles won',
            tackle_lost: 'Tackles lost',
            duel_aerial_won: 'Aerial duels won',
            duel_aerial_lost: 'Aerial duels lost',
            sub_off: 'Substitutions off',
            sub_on: 'Substitutions on',
            foul_committed: 'Fouls committed',
            punches: 'Punches',
            yellow_card: 'Yellow cards',
            second_yellow: 'Second yellow cards',
            red_card: 'Red cards',
            foul_given: 'Fouls given',
            man_of_the_match: 'Man of the match',
        }
    };

    constructor() {
        super('./data/soccer-players.json');

        [Graph.NODE, Graph.EDGE].forEach(entity => {
            Object.entries(SoccerGraph.LABELS[entity]).forEach(e => {
                const [attr, label] = e;
                this.labels[entity][attr] = label;
            })
        });
    }
}

export class RandomGraph extends Graph {

    constructor(numNodes, numEdges) {
        super();
        this.numNodes = numNodes;
        this.numEdges = numEdges;
    }

    load() {
        return new Promise(resolve => {
            // Create nodes
            let nodes = [];
            for (let i = 0; i < this.numNodes; i++) {
                nodes.push({
                    id: i,
                    x: Math.random() * 100,
                    y: Math.random() * 100,
                    attr_1: Math.random(),
                    attr_2: Math.random(),
                    attr_3: Math.random(),
                    attr_4: Math.random(),
                });
            }

            // Create edges
            let edges = [];
            for (let i = 0; i < this.numEdges; i++) {
                const srcIdx = Math.round(Math.random() * (this.numNodes - 1));
                let dstIdx = srcIdx;
                while (dstIdx === srcIdx)
                    dstIdx = Math.round(Math.random() * (this.numNodes - 1));

                edges.push({
                    src: srcIdx,
                    dst: dstIdx,
                    val: Math.random()
                });
            }
            this.prepare(nodes, edges);
            this.labels[Graph.NODE]['attr_1'] = "Attribute 1";
            this.labels[Graph.NODE]['attr_2'] = "Attribute 2";
            this.labels[Graph.NODE]['attr_3'] = "Attribute 3";
            this.labels[Graph.NODE]['attr_4'] = "Attribute 4";
            this.labels[Graph.EDGE]['val'] = "Edge Value";
            resolve(this);
        });
    }
}
