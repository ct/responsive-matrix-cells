import {Viewport} from "./Viewport.js";
import {Grid} from "./Grid.js";

export class AppBase {

    static KEY_EVENTS = ['keypress', 'keydown'];
    static MOUSE_EVENTS = ['mousedown', 'mouseup', 'mousemove', 'wheel', 'click', 'dblclick'];
    static TOUCH_EVENTS = ['touchstart', 'touchend', 'touchmove', 'touchcancel'];

    static EVENTS = [...AppBase.KEY_EVENTS, ...AppBase.MOUSE_EVENTS, ...AppBase.TOUCH_EVENTS];

    constructor(containerId, options) {
        const container = document.getElementById(containerId);
        this._init(container, options);
    }

    get interactables() {
        throw "App must implement interactables getter.";
    }

    load(graph) {
        graph.load()
            .then(graph => {
                this.data(graph);
            })
            .catch(error => {
                console.log(error);
            });
    }

    _init(container, options) {
        this.requestID = 0;
        container.innerHTML = "";
        this.canvas = document.createElement("canvas"); // Create canvas element
        this.canvas.tabIndex = -1; // Necessary to be able to receive key events for the canvas
        this.canvas.style.display = "block"; // To prevent overflow

        container.appendChild(this.canvas);
        this.gc = this.gc || this.canvas.getContext("2d");
        // this.gc.scale(window.devicePixelRatio, window.devicePixelRatio);

        this.window = (canvas => ({
            get x() {
                return 0;
            },
            get y() {
                return 0;
            },
            get width() {
                return canvas.width;
            },
            get height() {
                return canvas.height;
            }
        }))(this.canvas);

        this.viewport = new Viewport(this.window);
        this.grid = new Grid(this.viewport);

        this.init(container, options); // Call init function of sub-class
        this.ui(container, options); // Call ui function of sub-class

        if (options.fullscreen) {
            window.addEventListener('resize', () => this._resize());
            this._resize();
        } else {
            this.canvas.width = options.width * window.devicePixelRatio;
            this.canvas.height = options.height * window.devicePixelRatio;
            this.canvas.style.width = options.width + 'px';
            this.canvas.style.height = options.height + 'px';
            this.resize(options.width, options.height);
        }

        const pixelRatioChanged = () => {
            this._resize(); // Simply trigger _resize so others can update themselves

            // Add new event listener to new media query that fires (only once!) when the current pixel ratio has changed
            window.matchMedia(`(resolution: ${window.devicePixelRatio}dppx)`).addEventListener("change", pixelRatioChanged, {once: true});
        };
        pixelRatioChanged();

        this.canvas.oncontextmenu = () => false; // By default, the context menu is suppressed

        let locked = null;
        AppBase.EVENTS.forEach(type => {
            const handler = 'on' + type;
            const isKeyEvent = AppBase.KEY_EVENTS.includes(type);
            const isMouseEvent = AppBase.MOUSE_EVENTS.includes(type);
            const isTouchEvent = AppBase.TOUCH_EVENTS.includes(type);

            if (isKeyEvent) {
                // Attach key event handlers to the canvas
                this.canvas.addEventListener(type, evt => {
                    // console.log(evt);
                    // evt.preventDefault();
                    if (evt.key === "Tab") evt.preventDefault(); // prevent focus change, so "Tab" can be used multiple times as event key

                    let interactables = [...this.interactables, this.viewport, this]; // Collect interactables from sub-classes and append viewport and this base class
                    let handledBy = null;
                    interactables.forEach(interactable => {
                        if (handledBy === null && interactable[handler] && interactable[handler](evt)) {
                            handledBy = interactable;
                        }
                    });
                    if (handledBy !== null) this.requestUpdate(); // Request an update if an interactable handled the event
                });
            } else if (isMouseEvent || isTouchEvent) {
                // Attach mouse event handler to the window
                window.addEventListener(type, evt => {
                    // console.log(evt);
                    if (evt.target === this.canvas) this.canvas.focus(); // Give the canvas the key focus to be able to receive key events
                    if (evt.target !== this.canvas && !locked) return; // Only treat events on the canvas, unless interaction has been locked, in which case locked should be the interactable that is responsible
                    evt.preventDefault();

                    const clientRect = this.canvas.getBoundingClientRect();
                    let clientX, clientY;

                    if (isMouseEvent) {
                        clientX = evt.clientX;
                        clientY = evt.clientY;
                    } else if (isTouchEvent) {
                        clientX = evt.changedTouches[0].clientX;
                        clientY = evt.changedTouches[0].clientY;
                    }

                    evt.screenCoords = [window.devicePixelRatio * (clientX - clientRect.left), window.devicePixelRatio * (clientY - clientRect.top)];
                    evt.worldCoords = this.viewport.unproject(evt.screenCoords);

                    let interactables = locked ? [locked] : [...this.interactables, this.viewport, this]; // Either treat only the locked interactable or all interactables
                    let handledBy = null;
                    interactables.forEach(interactable => {
                        if (handledBy === null && interactable[handler] && (locked || interactable.pick(evt)) && interactable[handler](evt)) {
                            handledBy = interactable;

                            // To be able to handle drag operations, the responsible interactable needs to be locked during the drag operation
                            // Without locking, an interactable could lose the drag operation as a call to pick might return false, which would indicate
                            // that the interactable is not interested in the event.

                            if ((type === 'mousedown' || type === 'touchstart') && !locked) locked = interactable; // Lock on interactable when a button goes down
                            if ((type === 'mouseup' || type === 'touchend') && locked === interactable) locked = null; // Unlock from interactable when button is released
                        }
                    });
                    this.requestUpdate(); // Request an update on every mouse event
                }, {passive: false}); // {passive: false} is required for evt.preventDefault() to work correctly when scrolling
            }
        });
    }

    _resize() {
        const wasEnlarged = (this.canvas.width < window.innerWidth || this.canvas.height < window.innerHeight);
        this.canvas.width = Math.round(window.innerWidth * window.devicePixelRatio);
        this.canvas.height = Math.round(window.innerHeight * window.devicePixelRatio);
        this.canvas.style.width = window.innerWidth + 'px';
        this.canvas.style.height = window.innerHeight + 'px';
        this.viewport.fitToView(this.viewport.bounds.slice(), false, wasEnlarged);

        this.resize(window.innerWidth, window.innerHeight); // Call resize function of sub-class

        this.requestUpdate();
    }

    data(graph) {
        throw "App must implement data(..).";
    }

    init(container, options) {
        throw "App must implement init(..).";
    }

    ui(container, options) {
        throw "App must implement ui(..).";
    }

    resize(width, height) {
        throw "App must implement resize(..).";
    }

    reset() {
        throw "App must implement reset().";
    }

    update(time) {
        throw "App must implement update(..).";
    }

    requestUpdate() {
        window.cancelAnimationFrame(this.requestID);
        this.requestID = window.requestAnimationFrame((time) => {
            let needUpdate = false;
            needUpdate = this.viewport.update(time) || needUpdate;
            needUpdate = this.update(time) || needUpdate;  // Call update function of sub-class

            // Request next animation frame if needed
            if (needUpdate) this.requestUpdate();
        });
    }

    refresh() {
        return this.requestUpdate.bind(this);
    }

    onkeypress(evt) {
        switch (evt.key) {
            case 'g':
                this.grid.active = !this.grid.active;
                return true;
            case 'r':
                this.reset();
                return true;
        }
        return false; // Event not consumed
    }
}
