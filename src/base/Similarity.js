export class Similarity {

    static compute(graph, attrs, mode = 'euclidean') {

        const euclidean = (a, b) => {
            const n = attrs.length;
            a = graph.normalize(a, attrs); // Normalize the values to [0..1]
            b = graph.normalize(b, attrs);

            // Summarize the squared distances between the normalized attribute values
            const distSq = attrs.reduce((Σ, attr) => {
                let d = a[attr] - b[attr];
                return Σ + d * d;
            }, 0);

            // As the distance d between normalized values can maximally be 1, the squared distance d * d cannot
            // be larger than 1. Hence, the sum of the squared distances distSq is at most n, the number of attributes.
            // The actual distance corresponds to the square root of the squared distance sqrt(distSq).
            // To get a normalized distance between [0..1], the actual distance sqrt(distSq) needs to be divided
            // by sqrt(n). Root laws allow us to reduce sqrt(distSq) / sqrt(n) to sqrt(distSq / n).
            // To turn the distance into a similarity, we simply us 1 - distance.

            return 1 - Math.sqrt(distSq / n);
        }

        const cosine = (a, b) => {
            a = graph.normalize(a, attrs);
            b = graph.normalize(b, attrs);

            const dot = (a, b) => attrs.reduce((Σ, attr) => Σ + a[attr] * b[attr], 0);

            // If a or b are null vectors, the cosine similarity is undefined resulting in NaN being returned
            return dot(a, b) / Math.sqrt(dot(a, a) * dot(b, b));
        }

        const fns = {
            'cosine': cosine,
            'euclidean': euclidean
        };

        const fn = fns[mode]; // The similarity function to use

        const sim = [];
        const n = graph.nodes.length;
        for (let i = 0; i < n; i++) {
            sim[i] = [];
        }
        for (let i = 0; i < n; i++) {
            for (let j = i; j < n; j++) {
                let s = fn(graph.nodes[i], graph.nodes[j]);
                sim[i][j] = {value: s};
                sim[j][i] = sim[i][j];
            }
        }

        return sim;
    }
}
