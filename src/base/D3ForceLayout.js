import * as d3 from "d3-force";

export function D3ForceLayout(nodelink) {

    let done = false;

    const sim = d3.forceSimulation()
        .force("repel", d3.forceManyBody())
        .force("attract", d3.forceLink())
        .force("centerX", d3.forceX())
        .force("centerY", d3.forceY());
    sim.stop();

    sim.nodes(nodelink.dots);
    sim.force("repel").strength(-0.1);
    sim.force("attract").links(nodelink.links);
    sim.force("attract").distance(1);

    this.fixNode = function (n, x, y) {
        sim.alpha(Math.min(1, sim.alpha() + 0.2));
        nodelink.dots[n].fx = nodelink.dots[n].x = x;
        nodelink.dots[n].fy = nodelink.dots[n].y = y;

        done = false;
    };

    this.unfixNode = function (n) {
        nodelink.dots[n].fx = null;
        nodelink.dots[n].fy = null;

        done = false;
    };

    this.jiggle = function (scale) {
        sim.alpha(Math.min(1, sim.alpha() + 0.2));
        let n = nodelink.dots.length;
        while (n--) {
            nodelink.dots[n].x += Math.random() * scale - scale / 2;
            nodelink.dots[n].y += Math.random() * scale - scale / 2;
        }

        done = false;
    };

    this.update = function (time) {
        if (done) return;
        sim.tick();
        done = sim.alpha() < sim.alphaMin();
        return !done;
    };
}
