export class Rectangle extends Array {

    #right; // Right
    #bottom; // Bottom

    constructor(x = 0, y = 0, w = 1, h = 1) {
        super(4);
        this.set(x, y, w, h);
    }

    get x() {
        return this[0];
    }

    set x(x) {
        this[0] = x;
    }

    get y() {
        return this[1];
    }

    set y(y) {
        this[1] = y;
    }

    get width() {
        return this[2];
    }

    set width(w) {
        this[2] = w;
        this.#right = this.left + w;
    }

    get height() {
        return this[3];
    }

    set height(h) {
        this[3] = h;
        this.#bottom = this.top + h;
    }

    get left() {
        return this[0];
    }

    set left(l) {
        this[0] = l;
        this.width = this.#right - l;
    }

    get right() {
        return this.#right;
    }

    set right(r) {
        this.#right = r;
        this.width = r - this.left;
    }

    get top() {
        return this[1];
    }

    set top(t) {
        this[1] = t;
        this.height = this.#bottom - t;
    }

    get bottom() {
        return this.#bottom;
    }

    set bottom(b) {
        this.#bottom = b;
        this.height = b - this.top;
    }

    setRectangle(r) {
        this.set(r.x, r.y, r.width, r.height);
    }

    set(x, y, w, h) {
        this.x = x;
        this.y = y;
        this.height = h;
        this.width = w;
    }
}