import chroma from "chroma-js";

export class Util {
    static autoExpand(min, max, classes = 1) {

        // Expand min max to reasonable range (see https://dx.doi.org/10.1117/12.766440)

        function orderOfMagnitude(value) {
            let n = 0;

            while (value <= 10) {
                value *= 10;
                n++;
            }

            while (value > 100) {
                value /= 10;
                n--;
            }

            return n;
        }

        function nextMultiple10(value) {
            if (value === 0) return 0;
            if (value < 10) return 10;

            let next = value;
            if (value % 10 !== 0) {
                next /= 10;
                next = Math.round(next) * 10;
                if (next < value) next += 10;
            }
            return next;
        }

        function nextMultiple2(value) {
            let next = Math.round(value);
            if (next % 2 !== 0) next++;
            if (next < value) next += 2;
            return next;
        }

        // Calc range from min to max
        let range = max - min;
        // Get n so that range*pow(10,n)>=10 (order of magnitude of range)
        const n = (range > 0) ? orderOfMagnitude(range) : 0;
        // Shift min and max to order of magnitude of range
        max = max * Math.pow(10, n);
        min = min * Math.pow(10, n);
        // Get multiple of 10 for max
        max = nextMultiple10(max);

        // Calc range with new max
        range = Math.abs(max - min);

        // ORIGINAL step 4. from TTTL paper
        // // Calc range for one interval
        // let classRange = range / classes;
        // // Get multiple of 2 for class range
        // classRange = nextMultiple2(classRange);
        // // Set range min to new min so that the range of one interval is multiple of 2

        // OVERRIDE behavior of step 4. described in TTTL paper, use simple ceiling instead of looking for multiple of 2
        const classRange = Math.ceil(range / classes);
        min = max - classRange * classes;

        // Shift back
        max *= Math.pow(10, -n);
        min *= Math.pow(10, -n);

        return [min, max];
    };

    static projectPointToLineParametric(p, a, b, clamp = true) {
        // p - Point to be projected onto line
        // a - Start point of line
        // b - End point of line

        const dx = b[0] - a[0];
        const dy = b[1] - a[1];
        const l2 = dx * dx + dy * dy; // Length squared

        let t = (l2 > 0) ? ((p[0] - a[0]) * dx + (p[1] - a[1]) * dy) / l2 : 0;
        if (clamp) {
            if (t < 0) t = 0;
            if (t > 1) t = 1;
        }
        return t;
    }

    static projectPointToLine(p, a, b, clamp = true) {
        // p - Point to be projected onto line
        // a - Start point of line
        // b - End point of line
        const dx = b[0] - a[0];
        const dy = b[1] - a[1];
        let t = this.projectPointToLineParametric(p, a, b, clamp);
        return [a[0] + t * dx, a[1] + t * dy];
    }

    static lengthSq(v) {
        return v[0] * v[0] + v[1] * v[1];
    }

    static length(v) {
        return Math.sqrt(this.lengthSq(v));
    }

    static distanceSq(p, q) {
        const dx = q[0] - p[0];
        const dy = q[1] - p[1];
        return dx * dx + dy * dy;
    }

    static distance(p, q) {
        return Math.sqrt(Util.distanceSq(p, q));
    }

    static angle(p, q) {
        const dx = q[0] - p[0];
        const dy = q[1] - p[1];
        let theta = Math.atan2(dy, dx); // range (-PI, PI]
        // theta *= 180 / Math.PI; // rads to degs, range (-180, 180]
        // if (theta < 0) theta = 360 + theta; // range [0, 360)
        return theta;
    }

    static keepModulo(t, mod) {
        return (mod + t % mod) % mod;
    }

    static contrastColor(color) {
        return (chroma(color).luminance() < .7) ? 'white' : '#505050';
    }

    static font(gc, style, minSize, prefSize, maxSize, fullyVisibleAtSize, color) {
        let size = Math.min(Math.max(prefSize, minSize), maxSize); // Clamp size
        const t = Math.max(0, Math.min((size - minSize) / (fullyVisibleAtSize - minSize), 1)); // Fade-in between minSize and fullyVisibleAtSize
        size *= window.devicePixelRatio; // Correct pixel device ratio

        gc.font = `${size}px ` + style;
        gc.fillStyle = chroma(color).alpha(t).hex();

        return size;
    }

    // src: https://www.freecodecamp.org/news/javascript-debounce-example/
    static debounce(func, timeout = 300) {
        let timer;
        return (...args) => {
            clearTimeout(timer);
            timer = setTimeout(() => {
                func.apply(this, args);
            }, timeout);
        };
    }
}
