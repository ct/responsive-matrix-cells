export const Constants = {
    ZOOM_WHEEL_FACTOR: 0.7,
    ZOOM_KEY_FACTOR: 0.7,
    MOVE_KEY_FACTOR: 0.25,

    LAYOUT_EXTENT: 15,

    DRAG_THRESHOLD: 5, // Pixels before drag is triggered

    get PICK_TOLERANCE() { return 5 * window.devicePixelRatio;}, // 5 pixels tolerance
    get PICK_TOLERANCE_SQ() { return Math.pow(5 * window.devicePixelRatio, 2);}, // 5 pixels tolerance squared

    DEFAULT_NODE_ATTRIBUTES: ['mins_played', 'appearance', 'shots_total', 'goals'],
    DEFAULT_EDGE_ATTRIBUTES: ['weight'],
};
