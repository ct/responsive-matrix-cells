import {ColorBrewer} from "./ColorBrewer.js";
import chroma from "chroma-js";

export class ColorSelector {

    #colorselector;
    #head;
    #bars;
    #label;
    #list;
    #brewerMaps = Object.keys(ColorBrewer).sort();

    constructor(container, init, id) {

        init = init || "Spectral";

        this.#colorselector = document.createElement("div");
        this.#colorselector.classList.add("colorselector");
        this.#colorselector.id = id;
        container.append(this.#colorselector);

        this.#head = document.createElement("div");
        this.#head.classList.add("colorselector-head");
        this.#head.onclick = function (evt) {
            if (document.querySelector("#" + id).classList.contains("colorselector-expand")) {
                document.querySelector("#" + id).classList.remove("colorselector-expand");
            } else {
                document.querySelector("#" + id).classList.add("colorselector-expand");
            }
        };
        this.#colorselector.appendChild(this.#head);

        this.#bars = document.createElement("i");
        this.#bars.classList.add("bars");
        this.#bars.classList.add("icon");
        this.#head.appendChild(this.#bars);

        this.#label = document.createElement("span");
        this.#label.classList.add("colorselector-label");
        this.#label.textContent = init;
        this.#head.style.backgroundImage = 'url("' + this.gradient(ColorBrewer[init]["7"]) + '")';
        this.#head.appendChild(this.#label);

        this.#list = document.createElement("div");
        this.#list.classList.add("colorselector-entries");
        this.#colorselector.appendChild(this.#list);

        this.#brewerMaps.forEach(function (colormapname) {
            let entry = document.createElement("div");
            entry.classList.add("colorselector-entry");
            entry.textContent = colormapname;
            this.#list.appendChild(entry);

            entry.style.backgroundImage = 'url("' + this.gradient(ColorBrewer[colormapname]["7"]) + '")';

            entry.onclick = function (evt) {
                this.#label.textContent = evt.target.textContent;
                if (document.querySelector("#" + id).classList.contains("colorselector-expand")) {
                    document.querySelector("#" + id).classList.remove("colorselector-expand");
                } else {
                    document.querySelector("#" + id).classList.add("colorselector-expand");
                }

                this.#head.style.backgroundImage = entry.style.backgroundImage;
            }.bind(this);
        }, this);
    }

    get list() {
        return this.#list;
    }

    gradient(colors, width, height) {
        const colorScale = chroma.scale(colors).mode('lab');
        width = width || 30;
        height = height || 1;
        let canvas = document.createElement("canvas");
        canvas.width = width;
        canvas.height = height;
        let gc = canvas.getContext("2d");

        for (let x = 0; x < canvas.width; x++) {
            gc.fillStyle = colorScale(x / canvas.width).hex();
            gc.fillRect(x, 0, 2, height);
        }

        return canvas.toDataURL();
    }
}