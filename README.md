# Responsive Matrix Cells

## Developers

###### Maintainer
* [Christian Tominski](https://vcg.informatik.uni-rostock.de/~ct), University of Rostock

###### Contributors
* [Philip Berger](https://vcg.informatik.uni-rostock.de/~pb), University of Rostock
* [Tom Horak](https://tom-horak.de), Technische Universität Dresden

## About 

*Responsive Matrix Cells* is about embedding responsive visual representations
into the cells of a matrix. In its lower left triangular part, the matrix shows
adjacency information, that is, the connectivity in a graph based on its edges.
The upper right triangular part visualizes the pair-wise similarity of graph
nodes based on their associated data attributes.

*Responsive Matrix Cells* can be created, adjusted, and dismissed interactively (see below).
Different visualizations are offered to be embedded into responsive matrix cells.
They are all responsive in that the automatically adjust their appearance and content.
Editing individual data attribute values is possible directly within the embedded
visualizations.

A detailed description of the rationale and design of *Responsive Matrix Cells*
is available in a research article:
[*Responsive Matrix Cells: A Focus+Context Approach for Exploring and Editing Multivariate Graphs*](https://vcg.informatik.uni-rostock.de/~ct/pub_files/Horak21Responsive.pdf). 

*Responsive Matrix Cells* have been developed in the context of the DFG-funded
project [**Visual Editing and Comparison of Multivariate Graphs Using Multiple Interactive Displays (GEMS 2.0)**](https://gepris.dfg.de/gepris/projekt/214484876).

## How to Build and Run?

1. Install dependencies

        npm install

2. Build package to 'dist' folder

        npm run build
    
3. Launch project
  * Launch browser via npm

        npm run start
    or
  * Copy 'dist' folder to some webserver
  

## Dependencies

The software makes use or is based on the following resources:

* [HTML Canvas](https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API)
* [iGraph.js](https://vcg.informatik.uni-rostock.de/~ct/software/iGraph.js/index.html)
* [chroma.js](https://gka.github.io/chroma.js)
* [d3-force](https://github.com/d3/d3-force)
* [Semantic UI](https://semantic-ui.com/)
* Icons by [Yusuke Kamiyamane](http://p.yusukekamiyamane.com/)

## How to Use?

#### Matrix Exploration
* Mouse wheel: Zoom view
* Mouse right drag: Move view
* R: Reset entire matrix

#### Responsive Matrix Cells

###### Create and dismiss

* Mouse left drag: Create **meta** cell
* Shift + left drag: Create **unit** cells
* Del or middle button click: Dismiss cell

###### Configure and adjust

* Mouse right click: Open configuration menu
* Drag cell corner: Adjust cell size
* Shift + drag cell corner: Expand or shrink cell
* Shift + mouse wheel: Resize cell horizontally
* Ctrl + mouse wheel: Resize cell vertically
* Shift + Ctrl + mouse wheel: Resize cell symmetrically

###### Short cuts
* Arrow keys left/right: Next/previous visualization technique
* Arrow keys up/down: Next/previous layout variant
* Space: Toggle cell type (**unit** cells <-> ** meta** cell)
* Tab: Toggle cell context and position (node attributes <-> edge attributes)

#### Direct Data Editing

* Click handle: Enter data value
* Drag handle: Step value editing
* Ctrl + drag handle: Continuous value editing