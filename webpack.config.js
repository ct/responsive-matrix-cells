const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");

module.exports = {
    entry: "./src/index.js",
    mode: "development",
    output: {
        filename: "bundle.js",
        path: path.resolve(__dirname, "dist"),
        clean: true,
    },
    module: {
        rules: [
            {
                test: /\.js$/i,
                loader: "babel-loader",
                options: {
                    rootMode: "upward",
                }
            },
            {
                test: /\.css$/i,
                use: ["style-loader", "css-loader"],
            },
            {
                test: /\.(png|svg|jpg|gif)$/i,
                use: ["file-loader"],
            },
         ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: "Responsive Matrix Cells",
            template: "./src/index.html",
            favicon: "./src/favicon.png",
        }),
        new CopyWebpackPlugin({
            patterns: [
                {from: "./src/icons", to: "icons"},
                {from: "./src/data", to: "data"},
            ]
        }),
    ],
    devtool: "source-map",
    devServer: {
        static: "./dist",
    },
};

